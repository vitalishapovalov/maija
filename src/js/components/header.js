/**
 * Header component.
 *
 * @module Header
 */

import {
  Resp,
  $window,
  css,
  $document,
  throttle,
  $header,
  $body
} from 'js/modules/dev/_helpers';
import Animation from '../modules/dev/Animation';
import Menu from './menu';

export default class Header {

  constructor() {
    this.$header = window.$header || $('header');
  }

  /**
   * Hide header on scroll down.
   *
   * @returns {Header}
   */
  initToggle() {
    let lastScrollTop = 0;
    const $header = this.$header;
    const delta = 5;
    const headerHeight = $header.height();

    const checkWithThrottle = throttle(() => {
      const scrollTop = $window.scrollTop();
      const menuIsClosed = Menu.state === 'closed';

      // not enough scroll!!..
      if (Math.abs(lastScrollTop - scrollTop) <= delta) return;

      // popup is opened
      if ($body.hasClass(css.preventHeaderToggle)) return;

      if (scrollTop > lastScrollTop && scrollTop > headerHeight && menuIsClosed) {
        $header.addClass(css.headerHidden);
      } else if (scrollTop + $window.height() < $document.height()) {
        $header.removeClass(css.headerHidden);
      }

      lastScrollTop = scrollTop;
    }, 50, this);

    $window.on('scroll', checkWithThrottle);

    return this;
  }

  /**
   * Initialize web-site's menu.
   *
   * @return {Header}
   */
  initMenu() {
    Menu.init();

    return this;
  }

  /**
   * Run header animation (reveal).
   *
   * @param {Number} [timeout] - function timeout in ms
   * @param {jQuery} [$header]
   */
  static initHeaderAnimation(timeout = 0.07, $header = $('header')) {
    const animationsParams = {
      // DOM elements
      $rightLine: $header.find('.header__animation_line-right'),
      $leftLine: $header.find('.header__animation_line-left'),
      $logo: $header.find('.header__logo'),
      $button: $header.find('.header__button'),
      $links: Resp.isDesk ? $header.find('.header__link') : null,
      $burger: $header.find('.header__burger'),

      // ease settings
      ease: {
        line1: Power0.easeNone,
        line2: Power2.easeOut,
        components: Power3.easeOut
      },

      // time settings
      time: {
        line1: 0.12,
        line2: 0.8,
        components: 0.64
      },
      linesDelay: 0.12,
      componentsTimeout: 0.84,
      linksTimeout: 1.3,
      linksStaggerDelay: 0.2
    };

    Animation.delay(timeout, () => Animation.revealHeader(animationsParams));
  }

  /**
   * Initialize header's scripts.
   *
   * @return {Header}
   */
  init() {
    return this
      .initMenu()
      .initToggle();
  }
}
