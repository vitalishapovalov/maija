/**
 * Website's menu module.
 *
 * @module Menu
 */

import {
  $window,
  $header,
  $body,
  $scrolledElements,
  css
} from '../modules/dev/_helpers';
import Animation from '../modules/dev/Animation';

export default class Menu {

  constructor() {
    this.$headerBurger = $header.find('.header__burger');
    this.$menu = $header.find('.header__links');
    this.$menuInner = this.$menu.find('.header__links_inner');

    this.scrollTop = 0;
    $body.data('menuState', 'closed');

    this.paramsForAnimation = {
      ease: Power4.EaseOut,
      time: 0.4,
      $menu: this.$menu,
      $menuInner: this.$menuInner,
      $burgerLine1: this.$headerBurger.find('.line-1'),
      $burgerLine2: this.$headerBurger.find('.line-2'),
      $burgerLine3: this.$headerBurger.find('.line-3'),
      $burgerLine4: this.$headerBurger.find('.line-4')
    };
  }

  /**
   * Get current menu state (opened || closed).
   *
   * @return {String}
   */
  static get state() {
    return $body.data('menuState');
  }

  /**
   * Actions performed before menu is opened.
   *
   * @returns {Menu}
   */
  beforeOpen() {
    this.scrollTop = $window.scrollTop();

    $body.data('menuState', 'opened');

    return this;
  }

  /**
   * Actions performed after menu is opened.
   *
   * @returns {Menu}
   */
  afterOpen() {
    $body
      .css('top', -this.scrollTop)
      .addClass(css.menuActive);

    return this;
  }

  /**
   * Actions performed before menu is closed.
   *
   * @returns {Menu}
   */
  beforeClose() {
    $body
      .css('top', '')
      .removeClass(css.menuActive);

    $scrolledElements
      .scrollTop(this.scrollTop);

    return this;
  }

  /**
   * Actions performed after menu is closed.
   *
   * @returns {Menu}
   */
  afterClose() {
    $.each([this.$menuInner, this.$menu], (index, $element) => {
      $element.css('visibility', 'hidden');
    });

    $body.data('menuState', 'closed');

    return this;
  }

  /**
   * Add 'onclick' event listener to burger-button.
   */
  static init() {
    const obj = new this;
    const beforeOpen = obj.beforeOpen.bind(obj);
    const afterOpen = obj.afterOpen.bind(obj);
    const beforeClose = obj.beforeClose.bind(obj);
    const afterClose = obj.afterClose.bind(obj);

    obj.$headerBurger.on('click tap', () => {
      if (obj.$headerBurger.hasClass(css.active)) {
        Animation.closeMenu(obj.paramsForAnimation, beforeClose, afterClose);
      } else {
        Animation.openMenu(obj.paramsForAnimation, beforeOpen, afterOpen);
      }

      obj.$headerBurger.toggleClass(css.active);
    });
  }
}
