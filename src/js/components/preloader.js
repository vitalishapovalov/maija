/**
 * Website's preloader module.
 *
 * @module Preloader
 */

import {
  $body,
  css
} from '../modules/dev/_helpers';
import Animation from '../modules/dev/Animation';

export default class Preloader {
  /**
   * Create new preloader instance.
   *
   * @param {jQuery} $preloader - DOM element to activate preloader on.
   */
  constructor($preloader = $('#main-preloader')) {
    const $leftBlock = $preloader.find('.preloader__block-left');
    const $rightBlock = $preloader.find('.preloader__block-right');

    this.paramsForAnimation = {
      // logo animation settings
      logo: {
        time: 2,
        ease: 'Preloader',
        $logo: $preloader.find('.preloader__logo'),
        $leftBlockLines: $leftBlock.find('.preloader__line'),
        $rightBlockLines: $rightBlock.find('.preloader__line')
      },

      // curtains settings
      curtains: {
        time: 0.65,
        ease: Power2.easeInOut,
        $leftCurtain: $preloader.find('.preloader__curtain-left'),
        $rightCurtain: $preloader.find('.preloader__curtain-right')
      }
    };

    CustomEase.create(this.paramsForAnimation.logo.ease, 'M0,0 C0.05,0 0.087,0.002 0.12,0.004 0.136,0.008 0.151,0.013 0.165,0.017 0.322,0.05 0.365,0.103 0.365,0.103 0.44,0.138 0.466,0.362 0.498,0.502 0.518,0.592 0.552,0.77 0.615,0.864 0.675,0.953 0.698,0.958 0.854,0.996 0.894,0.999 0.941,1 1,1');
  }

  /**
   * Run Preloader's animation.
   *
   * @param {Function} syncCallback - function performed when all content is loaded
   * @param {Function} asyncCallback
   * @returns {Promise} resolves when animation finished.
   */
  runAnimation(syncCallback, asyncCallback) {
    return new Promise(resolveOuter => {
      Animation.runPreloader(
        this.paramsForAnimation,
        (animationPartOne) => new Promise(async resolveInner => {
          const asyncPromise = asyncCallback();
          const animationPromise = animationPartOne();

          await Promise.all([asyncPromise, animationPromise]);

          syncCallback();
          resolveInner();
        }),
        () => {
          $body.removeClass(css.hasPreloader);
          resolveOuter();
        }
      );
    });
  }

  /**
   * Initialize preloader.
   *
   * @param {Function} syncCallback
   * @param {Function} asyncCallback
   * @returns {Promise}
   */
  static async initPreloader(syncCallback, asyncCallback) {
    const $preloader = $('#main-preloader');
    return await new Preloader($preloader).runAnimation(syncCallback, asyncCallback);
  }
}
