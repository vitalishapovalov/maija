/**
 * Custom tabs module.
 *
 * @module Tabs
 */

import {
  css,
  throttle,
  Resp,
  delay as pause
} from '../modules/dev/_helpers';
import Animation from '../modules/dev/Animation';

export default class Tabs {
  /**
   * Create custom tabs instance and initialize it.
   *
   * @param {jQuery} $tabsContainer
   * @param {Object} [settings]
   * @param {Number} [settings.throttleTimeout=1000]
   * @param {Number} [settings.initialTab]
   * @param {Boolean} [settings.tabsAnimation=true]
   * @param {String} [settings.tabsContentAnimation='slide']
   * @param {Number} [settings.animationTime=0.88]
   * @param {Object} [settings.animationEase=Power3.easeInOut]
   * @param {Boolean} [settings.setCurrentTab=true]
   * @param {Function} [settings.onTabChange=null]
   * @param {Function} [settings.initialSet=true]
   */
  constructor($tabsContainer, settings = {}) {
    this.$tabs = $tabsContainer.find('.c_tabs__option');
    this.$tabsContent = $tabsContainer.find('.c_tabs__content');

    this.$leftCurtain = $tabsContainer.find('.c_tabs__animation-left');
    this.$rightCurtain = $tabsContainer.find('.c_tabs__animation-right');
    this.$line = $tabsContainer.find('.c_tabs__line');

    this.initialSet = this.contentInitialSet =
      typeof settings.initialSet === 'boolean' ? settings.initialSet : true;
    this.currentTabIndex = this.prevTabIndex = settings.initialTab || $tabsContainer.data('initial-tab');
    this.throttleTimeout = settings.throttleTimeout || 880;
    this.tabsContentAnimation = settings.tabsContentAnimation || 'slide';
    this.tabsAnimation = typeof settings.tabsAnimation === 'boolean' ? settings.tabsAnimation : true;
    this.animationTime = settings.animationTime || 0.88;
    this.animationEase = settings.animationEase || Power3.easeInOut;
    this.setCurrentTab = typeof settings.setCurrentTab === 'boolean' ? settings.setCurrentTab : true;
    this.onTabChange = settings.onTabChange || null;

    // set current tab specified
    if (this.initialSet) this.setTab(this.currentTabIndex);

    // initialize
    this.init();
  }

  /**
   * Get jQuery object of the tab with specified index.
   *
   * @param {String} items
   * @param {Number} itemIndex
   * @return {jQuery}
   */
  getItem(items, itemIndex) {
    if (items === 'tab') {
      return this.$tabs.filter(`[data-value-index=${itemIndex}]`);

    } else if (items === 'tabContent') {
      return this.$tabsContent.filter(`[data-content-index=${itemIndex}]`);
    }
  }

  /**
   * Return current active tab index.
   *
   * @get
   * @return {Number}
   */
  get getCurrentTabIndex() {
    return this.currentTabIndex;
  }

  /**
   * Return previously active tab index.
   *
   * @get
   * @return {Number}
   */
  get getPrevTabIndex() {
    return this.prevTabIndex;
  }

  /**
   * Animate tab's header line.
   *
   * @param {Number} newActiveTabIndex
   * @returns {Tabs}
   */
  async animateTabLine(newActiveTabIndex) {
    let time;

    if (this.initialSet) {
      time = 0;
      this.initialSet = false;

      // wait for DOM to render, otherwise we get wrong values (10ms - random number)
      await pause(100);

    } else {
      time = this.animationTime;
    }

    const $newAtiveTab = this.getItem('tab', newActiveTabIndex);
    const newActiveTabWidth = $newAtiveTab.width();
    const newActiveTabOffset = $newAtiveTab.offset().left;
    const trackOffset = this.$tabs.parent().offset().left;

    const paramsForAnimation = {
      $element: this.$line,
      time,
      ease: this.animationEase,
      x: newActiveTabOffset - trackOffset,
      width: newActiveTabWidth
    };
    Animation.changeDotLinePosition(paramsForAnimation);

    return this;
  }

  /**
   * Set new tab's active state.
   *
   * @param {Number} newActiveTabIndex
   * @returns {Tabs}
   */
  setTabActiveState(newActiveTabIndex) {
    // remove active state from other tabs
    this.$tabs.removeClass(css.active);

    // set selected tab's active state
    this.getItem('tab', newActiveTabIndex).addClass(css.active);

    // animation
    if (this.tabsAnimation && Resp.isDesk) this.animateTabLine(newActiveTabIndex);

    return this;
  }

  /**
   * Set new tab's content active state.
   *
   * @param {Number} newActiveTabContentIndex
   * @returns {Tabs}
   */
  setTabContentActiveState(newActiveTabContentIndex) {
    let timeout = 0, time = this.animationTime;

    if (this.contentInitialSet) {
      timeout = time = 0;
      this.contentInitialSet = false;

    } else {
      timeout = 0.44;
    }

    // animation
    if (this.tabsContentAnimation === 'slide') {
      const paramsForAnimation = {
        $leftCurtain: this.$leftCurtain,
        $rightCurtain: this.$rightCurtain,
        time,
        ease: this.animationEase
      };
      Animation.animateCurtains(paramsForAnimation);

    } else {
      console.error('Wrong tab content animation type specified');
      return this;
    }

    // change active content with timeout specified by animation
    Animation.delay(timeout, () => {
      // remove active state from other tabs
      this.$tabsContent.removeClass(css.active);

      // set selected tab's active state
      this.getItem('tabContent', newActiveTabContentIndex).addClass(css.active);
    });

    return this;
  }

  /**
   * Set active tab (tab and tab's content).
   *
   * @param {Number} tabId
   * @returns {Tabs}
   */
  setTab(tabId) {
    // change active tab
    this.setTabActiveState(tabId);

    // change active tab content
    this.setTabContentActiveState(tabId);

    return this;
  }

  /**
   * Change active tab.
   *
   * @param {jQuery|HTMLElement|String} newActiveTab
   * @returns {Tabs}
   */
  changeTab(newActiveTab) {
    const $newAtiveTab = $(newActiveTab);
    const newActiveTabIndex = $newAtiveTab.data('value-index');

    // do nothing if clicked on active tab
    if (newActiveTabIndex === this.currentTabIndex) return this;

    // write current and previous tab indexes
    this.prevTabIndex = this.currentTabIndex;
    this.currentTabIndex = newActiveTabIndex;

    // set new active tab
    this.setTab(newActiveTabIndex);

    // if callback in settings
    typeof this.onTabChange === 'function' ? this.onTabChange() : null;

    return this;
  }

  /**
   * Initialize (bind event listeners).
   */
  init() {
    const _this = this;

    this.$tabs.on('mouseenter', throttle(function () {
      _this.changeTab.call(_this, this);
    }, this.throttleTimeout));
  }
}
