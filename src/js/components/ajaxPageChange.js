/**
 * AJAX page change handler (with URL control).
 *
 * @module AjaxPageChange
 */

import {
  css,
  $window,
  $breadcrumbs,
  bindMethods
} from '../modules/dev/_helpers';
import Animation from '../modules/dev/Animation';

export default class AjaxPageChange {

  /**
   * Create new AjaxPageChange instance with specified settings.
   *
   * @param {Object} settings
   * @param {jQuery} settings.$eventHandler
   * @param {jQuery} settings.$animationTarget
   * @param {String} [settings.clickEvents]
   * @param {String} [settings.popstateEvents]
   * @param {Function} [settings.beforeChange]
   * @param {Function} [settings.afterChange]
   * @return {AjaxPageChange}
   */
  constructor(settings = {}) {
    this.$activeBreadcrumb = $breadcrumbs.find(`.${css.active}`).find('span');
    this.opts = {
      clickEvents: 'click.pageChange tap.pageChange',
      popstateEvents: 'popstate.pageChange',
      ...settings
    };

    this.cachedContent = this.opts.$animationTarget.html();
    this.runningAnimation = null;

    // save context
    bindMethods.bind(this)(
      'handleClick',
      'handlePopstate'
    );

    return this;
  }

  /**
   * Initialize AjaxPageChange.
   *
   * @static
   * @param {Object} settings
   * @return {AjaxPageChange}
   */
  static init(settings) {
    return new this(settings).activate();
  }

  /**
   * Activate AjaxPageChange instance.
   *
   * @return {AjaxPageChange}
   */
  activate() {
    return this
      .saveInitialData()
      .bindEventListeners();
  }

  /**
   * Save initial data to history state.
   *
   * @return {AjaxPageChange}
   */
  saveInitialData() {
    const url = window.location.href;
    const breadCrumbText = this.$activeBreadcrumb.text();
    const activeElementIndex = this.opts.$eventHandler.active().index();
    const activeContent = this.cachedContent;

    this.set.url(url, { breadCrumbText, activeElementIndex, activeContent });

    return this;
  }

  /**
   * Bind main 'onclick' event listener, 'popstate' event listener.
   *
   * @return {AjaxPageChange}
   */
  bindEventListeners() {
    const { $eventHandler, clickEvents, popstateEvents } = this.opts;

    $window.on(popstateEvents, this.handlePopstate);

    $eventHandler.on(clickEvents, this.handleClick);

    return this;
  }

  /**
   * Change content after popstate.
   *
   * @param {Object} event
   */
  handlePopstate(event) {
    // no state data found, get back to the prev page
    if (!event.originalEvent.state) window.history.back();

    const {
      breadCrumbText,
      activeElementIndex,
      activeContent
    } = event.originalEvent.state;
    const { $eventHandler, afterChange } = this.opts;

    // set active content
    this.set
      .activeElement($eventHandler.eq(activeElementIndex))
      .breadCrumbText(breadCrumbText);

    // animate content
    this.animateContent(0).then(() => {
      this.set.content(activeContent);

      // run CB if it was set
      typeof afterChange === 'function' && afterChange.call(this);

      this.animateContent(1);
    });
  }

  /**
   * Change page/content/url after clicking specified element.
   *
   * @param {Object} event
   */
  handleClick(event) {
    const beforeChangeCB = this.opts.beforeChange;
    const $prevElement = this.opts.$eventHandler.active();
    const target = event.target;
    const $clickedElement = $(target.tagName === 'LI' ? target : target.parentNode);
    const isActive = $clickedElement.hasClass(css.active);
    const pageUrl = $clickedElement.data('page-url');
    const contentUrl = $clickedElement.data('content-url');
    const breadCrumbText = $clickedElement.data('bc-name');

    // do nothing if tab is already active or animation is running
    if (isActive || this.runningAnimation) return;

    // run CB if it was set
    typeof beforeChangeCB === 'function' && beforeChangeCB.call(this);

    // perform ajax
    this.ajax({ pageUrl, contentUrl, breadCrumbText, $clickedElement, $prevElement });
  }

  /**
   * Get content from the server, animate content,
   * change url, change active breadcrumb.
   *
   * @param {Object} params
   * @return {AjaxPageChange}
   */
  ajax(params) {
    const set = this.set;
    const {
      pageUrl,
      contentUrl,
      breadCrumbText,
      $clickedElement,
      $prevElement
    } = params;
    const activeElementIndex = $clickedElement.index();
    const afterChangeCB = this.opts.afterChange;

    $.ajax({
      url: contentUrl,
      method: 'get',
      dataType: 'html',
      beforeSend: () => {
        this.runningAnimation = this.animateContent(0);
        set.activeElement($clickedElement);
      },
      success: activeContent => {
        this.runningAnimation.then(() => set.content(activeContent));
        set
          .breadCrumbText(breadCrumbText)
          .url(pageUrl, { breadCrumbText, activeElementIndex, activeContent });
      },
      complete: () => {
        this.runningAnimation.then(() => {
          // run CB if it was set
          typeof afterChangeCB === 'function' && afterChangeCB.call(this);

          this.animateContent(1).then(() => this.runningAnimation = null);
        });
      },
      error: () => {
        alert('Connection problems');
        set.activeElement($prevElement);
      }
    });

    return this;
  }

  /**
   * Functions-setters (active tab, bc text, content, url).
   *
   * @get
   * @return {Function}
   */
  get set() {
    const _this = this;
    const { $eventHandler, $animationTarget } = _this.opts;

    // mark one element as active, disable others
    function activeElement($element) {
      $eventHandler.removeClass(css.active);
      $element.addClass(css.active);

      return this;
    }

    // change active breadcrumb's text
    function breadCrumbText(text) {
      _this.$activeBreadcrumb.text(text);

      return this;
    }

    // change main page's content
    function content(content) {
      try {
        _this.cachedContent = JSON.stringify($animationTarget.children().detach());
        $animationTarget.append(content);
      } catch (e) {
        console.error(e);
      }

      return this;
    }

    // set new page URL, write history state
    function url(url, { breadCrumbText, activeElementIndex, activeContent }) {
      activeContent = activeContent.replace(/ opacity: 0;/g, '');

      window.history.pushState({
        breadCrumbText,
        activeElementIndex,
        activeContent
      }, '', url);

      return this;
    }

    return { activeElement, breadCrumbText, content, url };
  }

  /**
   * Animate content's opacity.
   *
   * @param {Number} opacity
   * @return {Promise}
   */
  animateContent(opacity) {
    return new Promise(resolve => {
      Animation.tweenWithTimeout(this.opts.$animationTarget, 0, {
        time: 0.4,
        params: { onComplete: resolve, ease: Power2.easeInOut, css: { opacity } }
      });
    });
  }
}
