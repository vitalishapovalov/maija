/**
 * Website's common scripts.
 *
 * @module Common
 */

import {
  _csrf,
  currentPage,
  css,
  $window,
  $footer,
  $document,
  $body,
  isSafari,
  $scrolledElements,
  Resp,
  validateEmail,
  debounce,
  initSelect,
  initCustomDropdown,
  getRoundetMinutes
} from '../modules/dev/_helpers';
import Animation from '../modules/dev/Animation';
import Preloader from './preloader';
import Header from './header';
import Home from '../pages/home/home';
import objectFitImages from 'object-fit-images';
import ScrollReveal from 'scrollreveal';
import imagesLoaded from 'imagesloaded';
import PWA from '../modules/dev/PWA';
import 'vintage-popup';
import 'air-datepicker';

export class Common {
  /**
   * Cache data.
   */
  constructor() {
    this.$dropDowns = () => $('.c_dropdown');
    this.$selects = () => this.$dropDowns().filter('.c_dropdown-selectable');
    this.$inputs = () => $('.c_input input');
    this.$popupButtons = () => $('[data-popup-target]');

    this.SR = ScrollReveal();

    this.$bradcrumbs = $('.breadcrumbs');
    this.$bradcrumbsFirstCrumb = $('.breadcrumbs li:first a');
    this.$bradcrumbsActiveCrumb = $('.breadcrumbs > .active');
    this.$upButton = $('.button_up');
  }

  /**
   * Initialize 'object-fit-images' polyfill.
   *
   * @returns {Common}
   */
  initObjectFitImages() {
    objectFitImages();

    return this;
  }

  /**
   * Initialize 'imagesLoad' func.
   *
   * @return {Common}
   */
  initImagesLoad() {
    window.imagesLoaded = imagesLoaded;

    return this;
  }

  /**
   * Initialize PWA module.
   *
   * @returns {Common}
   */
  initPWA() {
    new PWA('/sw.js').init();

    return this;
  }

  /**
   * Initialize header's scripts.
   *
   * @return {Common}
   */
  initHeader() {
    new Header().init();

    return this;
  }

  /**
   * Initialize website's popups.
   * @TODO: set active data-attr
   *
   * @return {Common}
   */
  initPopups() {
    const _this = this;
    const $popupButtons = this.$popupButtons();

    // header fix (jumps when opening/closing popup - cuz of window.scrollTop change)
    const afterClose = () => $body.removeClass(css.preventHeaderToggle);

    // initialize datepicker (if exists), save headers position
    const beforeOpen = popup => {
      const { $popup } = popup;
      const $formSelects = $popup.find('.form-select');
      const $datepicker = $popup.find('.date-field input');
      const $datepicker2 = $popup.find('.date-field-2 input');
      const hasDatepicker = $datepicker.length > 0;
      const hasDatepicker2 = $datepicker2.length > 0;
      const hasSelects = $popup.find('.c_dropdown-selectable');
      const datepickeIsActivated = $datepicker.data('datepicker');

      // save header's position (restore after)
      $body.addClass(css.preventHeaderToggle);

      // popup has dropdowns (selects)
      if (hasSelects) _this.initDropdowns().initSelect();

      // if popup has datepicker and its not inited - init
      if (hasDatepicker && !datepickeIsActivated) {
        const datepicker = $datepicker.datepicker({
          view: 'years'
        }).data('datepicker');

        // hide datepicker on popup scroll
        $popup.on('scroll.datepicker', () => {
          if (datepicker.visible) datepicker.hide();
        });
      }

      // if popup has datepicker and its not inited - init
      if (hasDatepicker2 && !datepickeIsActivated) {
        _this.initDatePicker();
      }

      // if popup has select - initialize it
      if ($formSelects.length) {
        $formSelects.each(function () {
          const $formSelect = $(this);
          const $input = $formSelect.find('input');
          const $options = $formSelect.find('.c_dropdown__option');

          $options.on('click', function () {
            $input.val($(this).data('answer-id'));
          });
        });

        _this.initSalonAjax().initServiceAjax();
      }
    };

    // Initialize popups
    $popupButtons.popup({ beforeOpen, afterClose });

    return this;
  }

  /**
   * Fix footer styles on mobile (css-crutches).
   *
   * @returns {Common}
   */
  initFooterFix() {
    const $absoluteElement = $footer.find('.js-fix-absolute');
    const $staticElement = $footer.find('.js-fix-static');

    function fixFooter() {
      if (Resp.isMobile) {
        const absoluteElementHeight = $absoluteElement.height();
        const staticElementHeight = $staticElement.height();

        // move static element
        $staticElement.css('margin-top', `${absoluteElementHeight + 60}px`);

        // move absolutely positioned element
        $absoluteElement.css('top', `-${absoluteElementHeight + staticElementHeight + 33}px`);
      } else {
        $staticElement.css('margin-top', '');
        $absoluteElement.css('top', '');
      }
    }

    // fix on load
    $window.on('load', fixFooter);

    // fix on resize
    $window.resize(debounce(fixFooter));

    return this;
  }

  /**
   * Validate 'subscribe' field.
   *
   * @return {Common}
   */
  initSubscribeValidation() {
    const $subscribeForm = $('.footer__subscribe');
    const $message = $subscribeForm.find('.footer__separate_text');
    const $errorMessage = $subscribeForm.find('.c_input__error');
    const $inputBlock = $subscribeForm.find('.form-group');
    const $input = $subscribeForm.find('input');
    const url = $subscribeForm.data('url');

    $subscribeForm.on('submit', e => {
      e.preventDefault();

      const email = $input.val();
      const isEmail = validateEmail(email);

      if (!isEmail) return setErrorState();

      $.ajax({
        url,
        dataType: 'json',
        method: 'POST',
        data: { _csrf, email },
        success: response => {
          const { success, message } = response;

          // error
          if (!success) return setErrorState(message);

          // success
          setSuccessState(message);
        }
      });
    });

    // set error state
    function setErrorState(message) {
      if (message) $errorMessage.text(message);

      $inputBlock.addClass(css.error);
    }

    // set success state
    function setSuccessState(message) {
      $message.text(message);

      $inputBlock.remove();

      $footer.addClass(css.footerSuccess);
    }

    return this;
  }

  /**
   * Initialize all custom dropdowns (c_dropdown).
   *
   * @returns {Common}
   */
  initDropdowns() {
    this.$dropDowns().each((index, dropDown) => {
      const $dropDown = $(dropDown);
      const $dropDownTitle = $dropDown.find('.c_dropdown__title');

      // should be inited only on mobile
      if ($dropDown.data('dropdown-mobile-only') && !Resp.isMobile) return;

      // shouldn't be inited twice
      if ($dropDown.data('c_dropdow')) return;

      // set flag
      $dropDown.data('c_dropdown', true);

      initCustomDropdown($dropDownTitle, $dropDown);
    });

    return this;
  }

  /**
   * Initialize 'UP' button.
   *
   * @returns {Common}
   */
  initUpButton() {
    this.$upButton.on('click tap', () => {
      $scrolledElements.animate({ scrollTop: 0 }, 1000, 'swing');
    });

    return this;
  }

  /**
   * Initialize 'select one from list' function on dropdowns.
   *
   * @returns {Common}
   */
  initSelect() {
    this.$selects().each((index, element) => {
      const $select = $(element);
      const $selected = $select.find('.c_dropdown__selected');
      const $option = $select.find('.c_dropdown__option');

      // shouldn't be inited twice
      if ($select.data('c_select')) return;

      // set flag
      $select.data('c_select', true);

      if (!Resp.isDesk) {
        $('.c_dropdown__options_inner').perfectScrollbar();
      }

      initSelect($select, $selected, $option, true);
    });

    return this;
  }

  /**
   * Initialize custom input (forms) scripts.
   *
   * @returns {Common}
   */
  initInputs() {
    const initializeInputs = function () {
      this.$inputs().each((index, input) => {
        const $input = $(input);

        // shouldn't be inited twice
        if ($input.data('c_input')) return;

        // set flag
        $input.data('c_input', true);

        $input.on({
          focusin: function () {
            setInputActiveState(input);
          },
          focusout: function () {
            unsetInputActiveState(input);
          }
        });

        if (input.value) setInputActiveState(input);
      });
    }.bind(this);

    function setInputActiveState(input) {
      $(input).parent().addClass(css.active);
    }

    function unsetInputActiveState(input) {
      const $this = $(input);
      const $formGroup = $this.parent();

      // remove active state if input is empty
      if (!$this.val()) $formGroup.removeClass(css.active);
    }

    //init inputs
    initializeInputs();

    // refresh on ready (placeholder animation)
    $document.ready(initializeInputs);

    return this;
  }

  /**
   * Initialize section's reveal on scroll.
   *
   * @return {Common}
   */
  initSectionsRevealOnScroll() {
    const srElementsSelector = `.${css.reveal}`;

    if (!$(srElementsSelector).length) return this;

    this.SR.reveal(srElementsSelector, {
      scale: 1,
      distance: '30px',
      duration: 700,
      easing: 'ease',
      mobile: false,
      viewOffset: {
        bottom: 70
      },
    });

    return this;
  }

  /**
   * Initialize 'Back' link on mobile devices (breadcrumbs).
   *
   * @return {Common}
   */
  initBreadcrumbs() {
    // no breadcumbs found
    if (!this.$bradcrumbs.length) return this;

    this.$bradcrumbs.on('click tap', () => {
      if (!Resp.isDesk) {
        this.$bradcrumbs.find('li').eq(0).hasClass('active') ?
          window.location.href = this.$bradcrumbsFirstCrumb.attr('href') :
          window.location.href = this.$bradcrumbsActiveCrumb.prev().find('a').attr('href');
      }
    });

    return this;
  }

  /**
   * Initialize safari-special fixes.
   *
   * @return {Common}z
   */
  initSafariFix() {
    if (!isSafari) return this;

    if (Resp.isDesk) {
      // page '404'
      const $container = $('.not-found__container');
      if ($container.length) $container.css({ top: 300 });

      // page 'Franchise'
      const $numbers = $('.numbers');
      if ($numbers.length) $numbers.addClass(css.active);
    }

    return this;
  }

  /**
   * @TODO refactor
   * @return {Common}
   */
  initMasterOnline() {
    const button = '#team__online';
    const salon = $('#salon__id').val();
    const _this = this;

    $document.on('click tap', button, function () {
      let person = $('#person__id').val();
      let url = $(this).data('url');

      $.ajax({
        url: url,
        method: 'POST',
        dataType: 'json',
        data: { _csrf, salon, person: person },
        success: function (response) {
          for (var i = 0, ilen = response.replaces.length; i < ilen; i++) {
            $(response.replaces[i].what).replaceWith(response.replaces[i].data);
          }
          _this.initDropdowns().initSelect().initDatePicker().initSalonAjax().initServiceAjax();
          $('.c_dropdown__option').on('click', function () {
            var $this = $(this);
            var $input = $this.parents('.c_dropdown').children('input');
            $input.val($this.data('answer-id'));
          });
        }
      });
    });

    return this;
  }

  /**
   * @TODO refactor
   * @return {Common}
   */
  initDatePicker() {
    const $datepicker = $('#onlinerecords-date');

    /**
     * Time picker minutes step
     * @type {number}
     */
    const step = 30;

    /** Work time */
    const defaultStartHours = 9;
    const finishHours = 20;

    /** Weekend work Time */
    const weekendStartHours = 11;
    const weekendEndHours = 19;

    let start = new Date(), prevDay, startHours;
    let currHours = start.getHours();
    let currDay = start.getUTCDate();

    if (currHours == finishHours || currHours == weekendEndHours) {
      start = new Date(start.getTime() + 1000 * 60 * 60 * 24);
      startHours = defaultStartHours;
    } else {
      startHours = currHours++;
    }

    start.setHours(startHours);
    start.setMinutes(0);

    $datepicker.datepicker({
      timepicker: true,
      classes: 'online-datepicker',
      startDate: start,
      minHours: startHours,
      maxHours: finishHours,
      minDate: start,
      minutesStep: step,
      onSelect: function (fd, d, picker) {
        if (!d) return;
        let day = d.getDay();
        if (prevDay && prevDay == day) return;
        prevDay = day;
        day == 6 || day == 0 ?
          picker.update({
            minHours: weekendStartHours,
            maxHours: weekendEndHours
          })
          : currDay < d.getUTCDate() ?
          picker.update({
            minHours: defaultStartHours,
            maxHours: finishHours
          })
          :
          picker.update({
            minHours: startHours,
            maxHours: finishHours,
            minMinutes: start.setMinutes(getRoundetMinutes(step))
          });
      }
    }).data('datepicker');

    return this;
  }

  /**
   * @TODO refactor
   * @return {Common}
   */
  initSalonAjax() {
    const $masterBlock = $('.master_item');
    const $masterParent = $masterBlock.parents('.c_dropdown');
    const $defaultSelect = $masterBlock.parents('.c_dropdown').find('.c_dropdown__selected');
    const defaultSelectValue = $defaultSelect.text();

    $document.on('click', '.salon_item > ul > li , .service_item  > ul > li', function () {
      let salonId = $('#onlinerecords-salon_id').val();
      let serviceId = $('#onlinerecords-service_id').val();
      let url = $('.salon_item').data('url');

      $defaultSelect.text(defaultSelectValue);
      $masterParent.find('input').val('');
      if (salonId.length > 0 && serviceId.length > 0) {
        $.ajax({
          url: url,
          method: 'POST',
          data: { _csrf, salon: salonId, service: serviceId },
          success: function (data) {
            $masterBlock.html('');
            $masterBlock.append(data);

            const $select = $masterBlock.parents('.c_dropdown-selectable');
            const $selected = $select.find('.c_dropdown__selected');
            const $option = $select.find('.c_dropdown__option');

            initSelect($select, $selected, $option, true);
            $('.c_dropdown__options_inner').perfectScrollbar();
            const $options = $select.find('.c_dropdown__option');
            const $input = $select.find('input');

            $options.on('click', function () {
              $input.val($(this).data('answer-id'));
            });
          }
        });
      }
    });

    return this;
  }

  /**
   * @TODO refactor
   * @return {Common}
   */
  initServiceAjax() {
    const $priceBlock = $('.price_item');
    const $priceParent = $priceBlock.parents('.c_dropdown');
    const $defaultSelect = $priceBlock.parents('.c_dropdown').find('.c_dropdown__selected');
    const defaultSelectValue = $defaultSelect.text();

    $document.on('click', '.service_item  > ul > li', function () {
      $defaultSelect.text(defaultSelectValue);
      $priceParent.find('input').val('');
      let serviceId = $('#onlinerecords-service_id').val();
      let url = $('.service_item').data('url');
      if (serviceId.length > 0) {
        $.ajax({
          url: url,
          method: 'POST',
          data: { _csrf, service: serviceId },
          success: function (data) {
            $priceBlock.html('');
            $priceBlock.append(data);

            const $select = $priceBlock.parents('.c_dropdown-selectable');
            const $selected = $select.find('.c_dropdown__selected');
            const $option = $select.find('.c_dropdown__option');

            initSelect($select, $selected, $option, true);
            $('.c_dropdown__options_inner').perfectScrollbar();
            const $options = $select.find('.c_dropdown__option');
            const $input = $select.find('input');

            $options.on('click', function () {
              $input.val($(this).data('answer-id'));
            });
          }
        });
      }
    });

    return this;
  }

  /**
   * Wait for window, video and images to load
   *
   * @return {Promise.<void>}
   */
  preloadHomePageContent() {
    const loading = [
      new Promise(resolve => $window.on('load', resolve)),
      new Promise(resolve => window.imagesLoaded($body, resolve))
    ];

    if (!Resp.isMobile) {
      const videos = [...$('.main_screen_slider video[preload="auto"]')];
      const loadingVideos = videos.reduce((acc, video) => {
        acc.push(
          new Promise(resolve => {
            let loop, loopOut;
            (function checkLoad() {
              if (video.readyState === 4) {
                clearTimeout(loopOut);
                resolve();
              } else {
                loop = setTimeout(checkLoad, 100);
              }
            }());

            loopOut = setTimeout(() => {
              clearTimeout(loop);
              resolve();
            }, 7000);
          })
        );
        return acc;
      }, []);

      loading.push(loadingVideos);
    }

    return Promise.all(loading);
  }

  /**
   * Reveal DOM, run preloader / home page animation if needed.
   *
   * @return {Promise.<void>}
   */
  revealDOM() {
    const hasPreloader = $body.hasClass(css.hasPreloader);
    const isHomePage = currentPage === 'home';

    return new Promise(async resolve => {
      if (hasPreloader) {
        // preloader detected, run animation
        await Preloader.initPreloader(
          Header.initHeaderAnimation,
          isHomePage ? () => this.preloadHomePageContent() : () => new Promise(resolve => resolve())
        );

        // reveal page
        if (!isHomePage) await new Promise(resolve => {
          TweenMax.fromTo('main', 1, {
            opacity: 0
          }, {
            opacity: 1,
            onComplete: resolve
          });
        });
      } else {
        await Promise.all([
          new Promise(resolve => window.imagesLoaded($body, resolve)),
          new Promise(resolve => $window.on('load', resolve))
        ]);

        // other pages, animate opacity
        Animation.tweenWithTimeout($body, 0, {
          time: 1,
          delay: 1,
          params: { ease: Power3.easeInOut, css: { opacity: 1 } }
        });
      }

      // main page detected, run animation
      if (isHomePage) {
        await new Promise(resolve => setTimeout(resolve, 50));
        await Home.reveal();
      }

      resolve();
    });
  }

  /**
   * @this Common
   * @return {Common}
   */
  checkHashAndOpenPopup = () => {
    const hash = window.location.hash;
    const hasMaijaPopup = hash.includes('online-record');

    if (!hasMaijaPopup) return this;

    const $headerRecordButton = $('.header__button');
    $headerRecordButton.click();

    return this;
  };

  /**
   * Initialize common scripts.
   */
  init() {
    this
      .initPWA()
      .initObjectFitImages()
      .initImagesLoad()
      .initHeader()
      .initPopups()
      .initDropdowns()
      .initMasterOnline()
      .initInputs()
      .initSelect()
      .initBreadcrumbs()
      .initUpButton()
      .initFooterFix()
      .initSubscribeValidation()
      .initSafariFix()
      .initSectionsRevealOnScroll();

    this
      .revealDOM()
      .then(this.checkHashAndOpenPopup);

    return this;
  }
}

/** Export initialized common scripts by default */
export default new Common().init();
