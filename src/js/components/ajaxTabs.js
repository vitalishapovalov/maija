/**
 * Custom tabs module with ajax data change, items upload.
 * @TODO refactor, options, callback
 *
 * @module AjaxTabs
 */

import {
  Resp,
  $scrolledElements,
  $window,
  _csrf
} from '../modules/dev/_helpers';
import Animation from '../modules/dev/Animation';
import Masonry from 'masonry-layout';
import Tabs from './tabs';

export default class AjaxTabs extends Tabs {
  /**
   * Create custom tabs with ajax change instance and initialize it.
   *
   * @param {jQuery} $tabsContainer
   */
  constructor($tabsContainer) {
    super($tabsContainer, {
      initialTab: $tabsContainer.data('initial-tab'),
      initialSet: false
    });

    // rewrite defaults
    this.initialSet = true;
    this.isAnimating = false;
    this.rightClass = 'c_tabs__ajax-item--right';
    this.initialLoad = true;

    // DOM nodes
    this.$tabsContainer = $tabsContainer;
    this.$tabsContentContainer = $tabsContainer.find('.c_tabs__body_content');
    this.$tabsContent = $tabsContainer.find('.c_tabs__ajax-item');
    this.$tabsButton = () => $tabsContainer.find('.c_tabs__button');
    this.$searchedWord = $('.main_screen__text_results').find('b');

    // set active tab
    this.setTabActiveState(this.currentTabIndex);

    // create grid layout
    this.updateGrid();

    // re-initialize
    this.reInit();
  }

  /**
   * Get jQuery object of the tab with specified index.
   *
   * @param {String} items
   * @param {Number} itemIndex
   * @return {jQuery}
   */
  getItem(items, itemIndex) {
    return this.$tabs.filter(`[data-label=${itemIndex}]`);
  }

  /**
   * Update grid layout (Masonry.js).
   *
   * @param {Function} [func] performed after layout is finished
   * @return {AjaxTabs}
   */
  async updateGrid(func) {
    // do nothing on mobile
    if (Resp.isMobile) return this;

    let imagesLoading = Promise.resolve();

    if (this.initialLoad) {
      this.initialLoad = false;

      imagesLoading = new Promise(resolve => {
        imagesLoaded(this.$tabsContainer, resolve);
      });
    }

    // wait for images to load
    await imagesLoading;

    const $container = this.$tabsContainer.find('.c_tabs__body_inner');

    // create layout instance
    this.grid = new Masonry($container.get(0), {
      percentPosition: true,
      itemSelector: '.c_tabs__ajax-item',
      initLayout: false
    });

    // bind events
    this.grid.on('layoutComplete', laidOutItems  => {
      typeof func === 'function' && func();

      laidOutItems.forEach(el => {
        const $el = $(el.element);

        $el.removeClass(this.rightClass);

        if (el.position.x > 0) $el.addClass(this.rightClass);
      });
    });

    // initialize layout
    this.grid.layout();

    return this;
  }

  /**
   * Set new tab's content active state.
   *
   * @param {Number} newActiveTabContentIndex
   * @param {jQuery|HTMLElement|String} newActiveTab
   * @returns {AjaxTabs}
   */
  setTabContentActiveState(newActiveTabContentIndex, newActiveTab) {
    const _this = this;
    const $newActiveTab = $(newActiveTab);
    const contentUrl = $newActiveTab.data('content-url');
    const { $tabsContainer, $tabsContentContainer } = _this;
    const $tabsContent = $tabsContentContainer.children();
    const data = { _csrf };
    let tabsAnimation;

    // special for 'search results' page
    if (this.$searchedWord.length) data.searchedWord = this.$searchedWord.text();

    // get data from the server
    $.ajax({
      url: contentUrl,
      method: 'post',
      dataType: 'html',
      data,

      beforeSend: () => {
        tabsAnimation = animateTabsContent(0);
        _this.isAnimating = true;
        $tabsContainer.css({ 'min-height': $tabsContainer.css('height') });
      },

      success: processResponse,

      error: () => {
        _this.setTabActiveState(_this.prevTabIndex);
        showContent();
      }
    });

    // process response, append data, update grid
    function processResponse(response) {
      tabsAnimation.then(() => {
        // remove old content
        $tabsContent.remove();

        // append new content
        $tabsContentContainer.append(response);

        // re-init masonry layout after images are loaded
        imagesLoaded($tabsContainer, async () => {
          // height crutch
          const fixHeight = () =>
            $tabsContainer.css({ 'min-height': $('.c_tabs__body_inner').css('height') });

          _this
            .reInit()
            .updateGrid(fixHeight);

          showContent();
        });
      });
    }

    // show / hide tabs content
    function animateTabsContent(opacity = 1) {
      return Animation.tweenWithTimeout(
        $tabsContentContainer,
        0,
        { time: 0.8, params: { css: { opacity } } }
      );
    }

    // show content (animate)
    function showContent() {
      tabsAnimation
        .then(animateTabsContent)
        .then(() => _this.isAnimating = false);
    }

    return this;
  }

  /**
   * Change active tab.
   *
   * @param {jQuery|HTMLElement|String} newActiveTab
   * @returns {AjaxTabs}
   */
  changeTab(newActiveTab) {
    // do nothing if currently animating
    if (this.isAnimating) return this;

    const $newAtiveTab = $(newActiveTab);
    const newActiveTabIndex = $newAtiveTab.data('label');

    // do nothing if clicked on active tab
    if (newActiveTabIndex === this.currentTabIndex) return this;

    // write current and previous tab indexes
    this.prevTabIndex = this.currentTabIndex;
    this.currentTabIndex = newActiveTabIndex;

    // change active tab
    this.setTabActiveState(newActiveTabIndex);

    // change active tab content
    this.setTabContentActiveState(newActiveTabIndex, newActiveTab);

    return this;
  }

  /**
   * Upload and append new items.
   *
   * @return {AjaxTabs}
   */
  uploadContent() {
    const _this = this;
    const $button = this.$tabsButton();
    const $btnDots = $button.find('.btn__dots');
    const $contentContainer = this.$tabsContainer.find('.c_tabs__body_inner');
    const url = $button.data('content-url');
    const dataLabel = this.$tabs.active().data('label');
    const itemsCount = $contentContainer.children().length;
    const launchPreloader = () => {
      $btnDots.css({ transition: 'none' });
      Animation.tweenWithTimeout($btnDots, 0, {
        time: 1,
        params: { rotation: 360, repeat: -1, ease: Power0.easeNone }
      });
    };
    const stopPreloader = () => {
      Animation.kill($btnDots, ['transform', 'transition']);
    };

    // 'success' callback. load images, append content, update layout
    function processResponse(response) {
      const $items = $(response);
      const itemsCount = $items.filter('.c_tabs__ajax-item').length;
      const currentScrollTop = $window.scrollTop();

      // hide items
      $items.css({ opacity: 0 });

      // add to DOM
      $contentContainer.append($items);

      // preload images
      imagesLoaded($contentContainer, () => {

        // re-calc grid
        _this.updateGrid();

        // scroll to the uploaded content
        $scrolledElements.scrollTop(currentScrollTop);

        // show items
        $items.css({ opacity: 1 });

        // stop preloader
        stopPreloader();
      });

      // no content left
      if (itemsCount < 7) $button.remove();
    }

    // get new items from the server
    const data = { dataLabel, itemsCount, _csrf };
    if (this.$searchedWord.length) data.searchedWord = this.$searchedWord.text();

    $.ajax({
      url,
      data,
      dataType: 'html',
      method: 'post',
      beforeSend: launchPreloader,
      error: stopPreloader,
      success: processResponse
    });

    return this;
  }

  /**
   * Clear method.
   *
   * @return {AjaxTabs}
   */
  init() { return this; }

  /**
   * Initialize (bind event listeners).
   *
   * @return {AjaxTabs}
   */
  reInit() {
    const _this = this;
    const events = 'click.tabs tap.tabs';

    // bind tabs
    _this.$tabs
      .unbind(events)
      .on(events, function () {
        _this.changeTab.call(_this, this);
      });

    // bind 'show more' button
    _this.$tabsButton()
      .unbind(events)
      .on(events, () => _this.uploadContent());

    return this;
  }
}
