'use strict';

/**
 * Home page scripts.
 *
 * @module Home
 */

import {Main01} from './sections/01_main';
import {Main02} from './sections/02_backStage';
import {Main03} from './sections/03_salons';
import {Main04} from './sections/04_advantages';
import {Main05} from './sections/05_services';
import {Main06} from './sections/06_visit';
import {Main07} from './sections/07_gallery';
import {Main08} from './sections/08_bonuses';

export default class Home {

    constructor() {
        // initialize after construction
        this.init();
    }

    /**
     * Initialize all home page's sections scripts.
     *
     * @return {Home}
     */
    runSectionScripts() {
        Main01.init();
        Main02.init();
        Main03.init();
        Main04.init();
        Main05.init();
        Main06.init();
        Main07.init();
        Main08.init();

        return this;
    }

    /**
     * Dummy initial scroll to trigger reveal animation.
     *
     * @returns {Home}
     */
    initialScroll() {
        window.scrollTo(0, 0);

        return this;
    }

    /**
     * Reveal main page's 1st section.
     *
     * @static
     * @return {Promise}
     */
    static reveal() {
        return Main01.initRevealAnimation();
    }

    /**
     * Initialize Home page scripts.
     *
     * @return {Home}
     */
    init() {
        return this
            .runSectionScripts()
            .initialScroll();
    }
}
