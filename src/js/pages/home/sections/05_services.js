import {
  Resp,
  css
} from '../../../modules/dev/_helpers';
import Animation from '../../../modules/dev/Animation';
import Tabs from '../../../components/tabs';
import Salons from './03_salons';

export default class Services extends Salons {

  constructor() {
    super($('#services'));

    this.$tabLabels = this.$section.find('.c_tabs__content > span');
    this.$ornament = this.$section.find('.services__ornament');
  }

  /**
   * Initialize tabs.
   *
   * @returns {Services}
   */
  initTabs() {
    // tabs
    new Tabs(this.$tabs, { tabsAnimation: Resp.isDesk });

    return this;
  }

  /**
   * Reveal section's tabs.
   *
   * @param {Number} timeout
   * @override
   * @returns {Services}
   */
  revealPart2(timeout = 0) {
    const revealTabsAnimationsParams = {
      $line: this.$line,
      $tabsContentActive: this.$tabsContent.filter(`.${css.active}`),
      $tabsContent: this.$tabsContent,
      $tabsTitles: this.$tabsOptions,
      $sliderButtons: this.$sliderControls,
      $dropdownTitle: this.$dropdownTitle,
      fromX: '1300px',
      fromWidth: '2500px'
    };

    setTimeout(() => {
      // show tabs block
      Animation.revealTabs(revealTabsAnimationsParams);

      // show white label in tabs
      const $activeLabel = this.$tabsContent.filter(`.${css.active}`).find('span');
      Animation.tweenWithTimeout($activeLabel, 1.4, {
        time: 0.4,
        params: {
          css: { opacity: 1 },
          onComplete: () => Animation.setTween(this.$tabLabels, { opacity: 1 })
        }
      });
    }, timeout);

    return this;
  }

  /**
   * Clear method.
   *
   * @override
   * @returns {Services}
   */
  initSliders() {
    return this;
  }
}

/**
 * Main page's services section instance.
 *
 * @constant
 * @type {Services}
 */
export const Main05 = new Services();
