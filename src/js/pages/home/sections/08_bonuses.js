import Visit from './06_visit';

export default class Bonus extends Visit {

  constructor() {
    const $bonuses = $('#bonuses');
    super($bonuses);

    this.$section = $bonuses;
    this.$sectionMark = this.$section.find('.section_mark');
    this.$board = this.$section.find('.board');
    this.$anchor = this.$board.find('.board__inner');
  }
}

/**
 * Main page's bonus section instance.
 *
 * @constant
 * @type {Bonus}
 */
export const Main08 = new Bonus();
