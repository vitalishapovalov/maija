import {
  ActionOnScroll,
  css,
  Resp,
  $document
} from '../../../modules/dev/_helpers';
import Animation from '../../../modules/dev/Animation';
import RevealingSection from '../../../modules/dev/RevealingSection';

export default class BackStage extends RevealingSection {

  constructor() {
    super($('#back-stage'));

    this.$backStage = this.$section;

    this.$videoPreview = this.$backStage.find('.back_stage__video');
    this.$video = $('#back-stage-video');

    // for reveal animation
    this.$sectionMark = this.$backStage.find('.section_mark');
    this.$sectionTitle = this.$backStage.find('.back_stage__title');
    this.$sectionText = this.$backStage.find('.anim_static_text');
    this.$backDrop = this.$backStage.find('.backdrop__inner div');
    this.$videoContainer = this.$videoPreview.find('.video_block__inner');
    this.$videoLine = this.$videoPreview.find('.back_stage__video_animation');
    this.$ornament = this.$backStage.find('.back_stage__ornament');
  }

  /**
   * Initialize video play / close.
   *
   * @returns {BackStage}
   */
  initVideo() {
    const $videoContainer = this.$video;
    const $closeButton = $videoContainer.find('.video_block__video_close');
    const $video = $videoContainer.find('video');
    const video = $video[0];
    const $videoSource = $video.find('source');
    const videoSrc = $videoContainer.data('src');
    let srcFlag = false;

    // open on click
    this.$videoPreview.on('click tap', () => {
      $video.bind('ended', function () {
        setTimeout(function () {
          $closeButton.trigger('click');
        }, 500);
      });

      // open video
      $videoContainer.addClass(css.active);

      // check for src
      if (!srcFlag) {
        srcFlag = true;

        // add video's src
        $videoSource.attr('src', videoSrc);

        // load and play
        video.load();
        video.play();
      } else {
        // just play, if it was loaded
        video.play();
      }
    });

    // close on 'X' button click
    $closeButton.on('click tap', closeMenu.bind(this));

    // close on 'esc' press
    $document.on('keyup', e => {
      if (e.keyCode === 27 && $videoContainer.hasClass(css.active)) closeMenu();
    });

    // pause video and close it
    function closeMenu() {
      // pause video
      video.pause();

      // close video
      this.$video.removeClass(css.active);
    }

    return this;
  }

  /**
   * Initialize reveal animation (text, mark).
   *
   * @param {Number} [timeout = 0] - animation timeout
   * @returns {BackStage}
   */
  revealPart1(timeout = 0) {
    Animation.delay(timeout, () => {
      // section's mark
      Animation.revealSectionMark(this.$sectionMark);

      // section's static text
      Animation.revealTextBlock(this.$sectionText, {}, 0.8);
    });

    return this;
  }

  /**
   * Initialize reveal animation (title, video, backdrop).
   *
   * @param {Number} [timeout = 0] - animation timeout
   * @returns {BackStage}
   */
  revealPart2(timeout = 0) {
    const animationParams = {
      // video
      $videoContainer: this.$videoContainer,
      $videoLine: this.$videoLine,
      videoTime: 0.6,
      videoTimeout: 0.5,
      videoEase: Power2.easeInOut
    };

    Animation.delay(timeout, () => {
      // show title
      Animation.revealSectionTitle(this.$sectionTitle);

      // show backdrop
      Animation.runLine(this.$backDrop, {}, 0.2);

      // show video
      Animation.revealVideo(animationParams);

      // show ornament
      Animation.tweenWithTimeout(this.$ornament, 0.55, {
        time: 0.35,
        params: { css: { opacity: 0.05 } }
      });
    });

    return this;
  }

  /**
   * Initialize onscroll listener for reveal functions.
   *
   * @returns {BackStage}
   */
  initRevealOnScroll() {
    if (!Resp.isMobile) {
      new ActionOnScroll([
        {
          anchor: this.$sectionMark,
          action: this.revealPart1,
          context: this,
          fireOnLoad: Resp.isTablet,
          fireOnLoadTimeout: 3500,
        },
        {
          anchor: this.$sectionTitle,
          action: this.revealPart2,
          context: this,
          scrollOffset: 50
        }
      ]);
    }

    return this;
  }

  /**
   * Other section's scripts.
   *
   * @return {RevealingSection}
   */
  initOtherActions() {
    this.initVideo();

    return this;
  }
}

/**
 * Main page's behind the screen section instance.
 *
 * @constant
 * @type {BackStage}
 */
export const Main02 = new BackStage();
