import {
  Resp,
  ActionOnScroll,
  isIpad
} from '../../../modules/dev/_helpers';
import Animation from '../../../modules/dev/Animation';
import RevealingSection from '../../../modules/dev/RevealingSection';
import { Main05 as Services } from './05_services';

export default class Gallery extends RevealingSection {

  constructor() {
    super($('#gallery'));

    this.$sectionMark = this.$section.find('.section_mark');
    this.$sectionTitle = this.$section.find('.gallery__title');
    this.$photos = this.$section.find('.photos');
    this.$backDrop = this.$section.find('.backdrop__inner > div');
    this.$ornament = Services.$ornament;
  }

  /**
   * Initialize reveal animation (title, mark).
   *
   * @param {Number} [timeout = 0] - animation timeout
   * @returns {Gallery}
   */
  revealPart1(timeout = 0) {
    Animation.delay(timeout, () => {
      // section's mark
      Animation.revealSectionMark(this.$sectionMark);

      // section's static text
      Animation.revealSectionTitle(this.$sectionTitle, {}, 0.6);
    });

    return this;
  }

  /**
   * Initialize reveal animation (title, mark).
   *
   * @param {Number} [timeout = 0] - animation timeout
   * @returns {Gallery}
   */
  revealPart2(timeout = 0.2) {
    Animation.delay(timeout, () => {
      // reveal photos
      Animation.revealPhotos(this.$photos);

      // show backdrop
      Animation.runLine(this.$backDrop, {}, 0.2);

      // show ornament
      Animation.tweenWithTimeout(this.$ornament, 2, {
        time: 0.65,
        params: { css: { opacity: 0.05 } }
      });
    });

    return this;
  }

  /**
   * Initialize onscroll listener for reveal functions.
   *
   * @returns {Gallery}
   */
  initRevealOnScroll() {
    if (!Resp.isMobile) {
      new ActionOnScroll([
        {
          anchor: this.$sectionMark,
          action: this.revealPart1,
          context: this
        },
        {
          anchor: this.$photos,
          action: this.revealPart2,
          context: this,
          scrollOffset: 400
        }
      ]);
    }

    return this;
  }

  /**
   * Other section's scripts.
   * 1) fix styles on iPad
   *
   * @return {RevealingSection}
   */
  initOtherActions() {
    // ipad styles
    if (isIpad) {
      const $rightBottomPhotosTitle =
        this.$section.find('.photos__photo-secondary-bottom .photos__date');

      $rightBottomPhotosTitle.css('top', '105px');
    }
  }
}

/**
 * Main page's gallery section instance.
 *
 * @constant
 * @type {Gallery}
 */
export const Main07 = new Gallery();
