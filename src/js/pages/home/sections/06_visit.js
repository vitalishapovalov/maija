import {
  $window,
  Resp,
  throttle
} from '../../../modules/dev/_helpers';
import Animation from '../../../modules/dev/Animation';
import RevealingSection from '../../../modules/dev/RevealingSection';

export default class Visit extends RevealingSection {

  constructor() {
    super($('#visit'));

    this.$sectionMark = this.$section.find('.section_mark');
    this.$board = this.$section.find('.board');
    this.$anchor = this.$board.find('.board__inner');
  }

  /**
   * Reveal animation steps.
   * Should return object with animation functions (maximum - 5 steps).
   *
   * @return {Object}
   */
  animationSteps() {
    const _this = this;
    const animationParams = { width: '1048px' };

    // special set-up for tablet devices
    if (Resp.isTablet) {
      const getInnerWidth = () => _this.$board.find('.board__inner').outerWidth();
      animationParams.width = getInnerWidth();

      // update width on resize
      $window.on('resize', throttle(() => _this.$board.css('width', getInnerWidth())));
    }

    function one() {
      Animation.delay(0, () => {
        // section's mark
        Animation.revealSectionMark(_this.$sectionMark);

        // reveal 'board' block
        Animation.revealBoard(_this.$board, animationParams);
      });
    }

    return { one };
  }
}

/**
 * Main page's bonus section instance.
 *
 * @constant
 * @type {Visit}
 */
export const Main06 = new Visit();
