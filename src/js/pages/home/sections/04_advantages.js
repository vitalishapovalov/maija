import {
  css,
  Resp,
  ActionOnScroll
} from '../../../modules/dev/_helpers';
import Animation from '../../../modules/dev/Animation';
import RevealingSection from '../../../modules/dev/RevealingSection';

export default class Advantages extends RevealingSection {

  constructor() {
    super($('#advantages'));

    this.$advantages = this.$section;

    this.$sectionMark = this.$anchor = this.$advantages.find('.section_mark');
    this.$sectionTitle = this.$advantages.find('.advantages__title');
    this.$items = this.$advantages.find('.advantages__item');
  }

  /**
   * Reveal animation steps.
   * Should return object with animation functions (maximum - 5 steps).
   *
   * @return {Object}
   */
  animationSteps() {
    const _this = this;

    function one() {
      Animation.delay(0, () => {
        // section's mark
        Animation.revealSectionMark(_this.$sectionMark);

        // show title
        Animation.revealSectionTitle(_this.$sectionTitle, {}, 0.6);

        // show items
        Animation.delay(1.35, () => {
          Animation.staggerDelay(_this.$items, 0.3, (element) => {
            $(element).addClass(css.textAnimationFinished);
          });
        });
      });
    }

    return { one };
  }
}

/**
 * Main page's advantages section instance.
 *
 * @constant
 * @type {Advantages}
 */
export const Main04 = new Advantages();
