import {
  ActionOnScroll,
  Resp,
  css
} from '../../../modules/dev/_helpers';
import Animation from '../../../modules/dev/Animation';
import Tabs from '../../../components/tabs';
import RevealingSection from '../../../modules/dev/RevealingSection';

export default class Salons extends RevealingSection {
  /**
   * Cache data.
   *
   * @param {jQuery} $section - jQuery object ('#salons' by default)
   */
  constructor($section = $('#salons')) {
    super($section);

    this.$sectionMark = this.$section.find('.section_mark');
    this.$sectionTitle = this.$section.find('.anim_section_title');
    this.$sectionText = this.$section.find('.anim_static_text');

    this.$tabs = this.$section.find('.c_tabs');
    this.$about = this.$section.find('.salons__about');

    this.$backdrop = this.$section.find('.backdrop__inner div');
    this.$dropdownTitle = this.$tabs.find('.c_dropdown__title');
    this.$tabsOptions = this.$tabs.find('.c_tabs__option');
    this.$tabsContent = this.$tabs.find('.c_tabs__content');
    this.$line = this.$tabs.find('.c_tabs__line');
    this.$soon = this.$section.find('.salons__soon');
    this.$soonLine1 = this.$soon.find('.salons__soon_line1');
    this.$soonLine2 = this.$soon.find('.salons__soon_line2');
    this.$soonLine3 = this.$soon.find('.salons__soon_line3');
  }

  /**
   * Initialize reveal animation (title, text, mark).
   *
   * @param {Number} [timeout = 0] - animation timeout
   * @returns {Salons}
   */
  revealPart1(timeout = 0) {
    Animation.delay(timeout, () => {
      // section's mark
      Animation.revealSectionMark(this.$sectionMark);

      // show title
      Animation.revealSectionTitle(this.$sectionTitle, {}, 0.6);

      // section's static text
      Animation.revealTextBlock(this.$sectionText, {}, 1.3);
    });

    return this;
  }

  /**
   * Initialize reveal animation (tabs, about, 'soon', backdrop).
   *
   * @param {Number} [timeout = 0] - animation timeout
   * @returns {Salons}
   */
  revealPart2(timeout = 0) {
    const revealTabsAnimationsParams = {
      $line: this.$line,
      $tabsContentActive: this.$tabsContent.filter(`.${css.active}`),
      $tabsContent: this.$tabsContent,
      $tabsTitles: this.$tabsOptions,
      $dropdownTitle: this.$dropdownTitle
    };

    Animation.delay(timeout, () => {
      // show tabs block
      Animation.revealTabs(revealTabsAnimationsParams);

      // show backdrop
      Animation.runLine(this.$backdrop, {}, 0.52);

      // show 'about' block
      const $about = this.$about;
      const $aboutBody = $about.find('.about__body');
      const $activeTab = $aboutBody.find(`[data-content-index=${this.TabsInst.getCurrentTabIndex}]`);
      const activeTabHeight = $activeTab.height();
      Animation.tweenWithTimeout($aboutBody, 1.4, {
        time: 0.64,
        params: {
          ease: Power2.easeInOut,
          css: { height: activeTabHeight },
          onComplete: showContentInAbout
        }
      });
      function showContentInAbout() {
        Animation.tweenWithTimeout($activeTab, 0, {
          time: 0.4,
          params: {
            ease: Power2.easeInOut,
            css: { opacity: 1, visibility: 'visible' }
          }
        });
      }

      // show 'soon' label
      Animation.delay(1.5, () => {
        Animation.tweenWithTimeout(this.$soon, 0, {
          time: 0.8,
          params: { css: { opacity: 0.2 } }
        });
        Animation.tweenWithTimeout(this.$soonLine1, 0.4, {
          time: 0.2,
          params: { css: { height: '40px' } }
        });
        Animation.tweenWithTimeout(this.$soonLine2, 0.6, {
          time: 0.2,
          params: { css: { height: '46px' } }
        });
        Animation.tweenWithTimeout(this.$soonLine3, 0.8, {
          time: 0.2,
          params: { css: { height: '40px' } }
        });
      });

    });

    return this;
  }

  /**
   * Initialize onscroll listener for reveal functions.
   *
   * @returns {Salons}
   */
  initRevealOnScroll() {
    if (!Resp.isMobile) {
      new ActionOnScroll([
        {
          anchor: this.$sectionMark,
          action: this.revealPart1,
          context: this
        },
        {
          anchor: this.$tabs,
          action: this.revealPart2,
          context: this
        }
      ]);
    }

    return this;
  }

  /**
   * Initialize tabs and content change in 'about company' block.
   *
   * @returns {Salons}
   */
  initTabs() {
    const $about = this.$about;
    const $aboutBody = $about.find('.about__body');
    const $aboutHead = $about.find('.about__head');

    // tabs
    const TabsInst = this.TabsInst = new Tabs(this.$tabs, {
      tabsAnimation: Resp.isDesk,

      // content change in 'about company' block
      onTabChange: () => {
        const $nextTab = getItem();
        const $prevTab = getItem(TabsInst.getPrevTabIndex);

        // change on hover. on mobile - 'about' block must be 'active'
        if ($about.hasClass(css.active) || !Resp.isMobile) {
          Animation.tweenWithTimeout($prevTab, 0, {
            time: 0.4,
            params: { css: { opacity: 0 } },
            onComplete: () => {
              Animation.setTween($prevTab, { visibility: 'hidden' });
            }
          });
          Animation.tweenWithTimeout($nextTab, 0, {
            time: 0.4,
            params: {
              delay: 0.4,
              css: { opacity: 1, visibility: 'visible' },
              onComplete: () => $prevTab.css({
                opacity: 0,
                visibility: 'hidden'
              })
            }
          });
          Animation.tweenWithTimeout($aboutBody, 0, {
            time: 0.4,
            params: { delay: 0.4, css: { height: $nextTab.height() } }
          });
        }
      }
    });

    // open / close 'about' block (mobile)
    if (Resp.isMobile) {
      $aboutHead.on('click tap', () => {
        if ($about.hasClass(css.active)) {
          Animation.tweenWithTimeout($aboutBody, 0, {
            time: 0.4,
            params: { css: { height: 0 } }
          });
          Animation.tweenWithTimeout(getItem(), 0, {
            time: 0.4,
            params: {
              css: { opacity: 0 },
              onComplete: () => {
                // remove active state
                $about.data('active', false);

                // remove hovered state if not hovered anymore
                if (!$about.data('hovered')) $about.removeClass(css.hovered);
              }
            }
          });

          // remove active state
          $about.removeClass(css.active);
        } else {
          Animation.tweenWithTimeout($aboutBody, 0, {
            time: 0.4,
            params: { css: { height: getItem().height() } }
          });
          Animation.tweenWithTimeout(getItem(), 0, {
            time: 0.4,
            params: { css: { opacity: 1 } }
          });

          // set active state
          $about
            .data('active', true)
            .addClass(css.active);
        }
      });
    }

    // get current content (jQuery object)
    function getItem(index = TabsInst.getCurrentTabIndex) {
      return $aboutBody.find(`[data-content-index=${index}]`);
    }

    return this;
  }

  /**
   * Other section's scripts.
   *
   * @return {RevealingSection}
   */
  initOtherActions() {
    this.initTabs();

    return this;
  }
}

/**
 * Main page's salon section instance.
 *
 * @constant
 * @type {Salons}
 */
export const Main03 = new Salons();
