import {
  $scrolledElements,
  Resp,
  css,
  initializeSlider,
  bindMethods
} from '../../../modules/dev/_helpers';
import Animation from '../../../modules/dev/Animation';

export default class MainScreen {

  constructor() {
    this.$mainScreen = $('#main-screen');

    this.$mainSlider = this.$mainScreen.find('.main_screen_slider__container');
    this.$scrollDownButton = this.$mainScreen.find('.main_screen_scrolldown');
    this.$mainScreenSocials = this.$mainScreen.find('.main_screen_socials');
    this.$mainScreenHot = this.$mainScreen.find('.main_screen_hot');
    this.$hotSlider = this.$mainScreenHot.find('.main_screen_hot__slider');
    this.$ornament = this.$mainScreen.find('.main_screen__ornament');

    // for reveal animation
    this.$socialsAnimation = this.$mainScreen.find('.main_screen_socials__animation');
    this.$mainPicAnimation = this.$mainScreen.find('.main_screen_slider__container');
    this.$languages = this.$mainScreen.find('.main_screen_languages');
    this.$sliderButtons = this.$mainScreen.find('.main_screen_slider__nav');
    this.$mainScreenText = this.$mainScreen.find('.main_screen_text');
    this.$mainScreenTitle = this.$mainScreen.find('.main_screen_title');

    // save context
    bindMethods.bind(this)(
      'initRevealAnimation'
    );
  }

  /**
   * Run home page's first section reveal animation.
   *
   * @param {Number} [timeout]- timeout before animation starts
   * @returns {Promise}
   */
  initRevealAnimation(timeout = 0.05) {
    const animationsParams = {
      // socials
      $socials: Resp.isDesk ? this.$socialsAnimation : false,
      socialTimeout: 0.2,

      // slider pic
      $sliderPic: this.$mainPicAnimation,
      sliderPicTimeout: 0.15,

      // slider buttons
      $sliderButtons: this.$sliderButtons,
      sliderButtonsTimeout: 0.845,
      sliderButtonsCss: Resp.isMobile ? { y: '0%' } : { width: '180px' },

      // slider dots
      $sliderDots: this.$mainSlider.find('.slider_custom__dots'),
      $sliderDotsTime: this.$mainSlider.find('.slider_custom__time'),
      sliderDotsTimeout: Resp.isMobile ? 1.1 : 1.45,
      sliderDotsCss: Resp.isDesk ? { width: '144px' } : { x: '0px', opacity: 1 },
      sliderDotsTimeClass: css.isHiddenForAnimation,

      // hot section
      $hotHidden: this.$mainScreenHot.find('.main_screen_hot__hidden'),
      hotTimeout: Resp.isDesk ? 0.75 : 0.4,

      // languages
      $languages: this.$languages,
      languagesTimeout: 0.36,

      // home screen text
      $mainScreenText: () => this.$mainScreenText.filter(`.${css.active}`),
      mainScreenTextTimeout: 1.1,

      // home screen title
      $mainScreenTitle: this.$mainScreenTitle,
      mainScreenTitleActiveClass: css.active,
      mainScreenTitleTimeout: Resp.isMobile ? 0.58 : 0.4,

      // home screen lines (socials, scrolDown)
      $mainScreenLines: [this.$mainScreenSocials, this.$scrollDownButton],
      mainScreenLinesTimeout: 0.48,

      // ornament
      $ornament: this.$ornament,
      ornamentTimeout: 2,

      // ease settings
      ease: {
        socials: Power1.easeOut,
        sliderPic: Resp.isDesk ? Power0.easeNone : Power1.easeOut,
        hot: Power2.easeOut,
        languages: Power3.easeOut,
        sliderButtons: Power2.easeOut,
        sliderDots: Power2.easeInOut
      },

      // time settings
      time: {
        socials: 0.6,
        sliderPic: Resp.isDesk ? 1.36 : 0.86,
        hot: 0.5,
        languages: 0.52,
        sliderButtons: Resp.isDesk ? 0.72 : 0.55,
        sliderDots: 0.5
      }
    };

    return Animation.revealMainScreen(animationsParams);
  }

  /**
   * Set title's animated (white) lines initial 'top' position.
   *
   * @private
   * @param {Number} activeTitleIndex
   */
  _setTitleAnimatedLinesPosition(activeTitleIndex) {
    const $leftLine = this.$mainScreenTitle.find('.anim_section_title__line-left');
    const $rightLine = this.$mainScreenTitle.find('.anim_section_title__line-right');
    const $activeTitle = this.$mainScreenTitle
      .find('.anim_section_title__text')
      .filter(`[data-related-slide-id=${activeTitleIndex}]`);
    const activeTitleOffsetTop = $activeTitle.offset().top;
    const titleOffsetTop = this.$mainScreenTitle.offset().top;
    const offsetTopDiff = titleOffsetTop - activeTitleOffsetTop;
    const isType2 = $activeTitle.hasClass('design-type-2');

    Animation.setTween($leftLine, { top: `-=${offsetTopDiff}` });

    if (isType2) {
      Animation.setTween($rightLine, { bottom: Resp.isDesk ? 45 : -9 });
    }
  }

  /**
   * Initialize home page's first section home slider.
   *
   * @returns {MainScreen}
   */
  initMainSlider() {
    const $mainScreenSliderParent = this.$mainSlider.parent();
    const $prevArrow = $mainScreenSliderParent.find('.slider_custom__prev');
    const $nextArrow = $mainScreenSliderParent.find('.slider_custom__next');
    const $titles = this.$mainScreen.find('.main_screen_title .h1');
    const $texts = this.$mainScreen.find('.main_screen_text');
    const initialSlide = this.$mainSlider.data('initial-slide');

    // functions-helpers
    const setActiveTitleAndText = (slideIndex, animate) => {
      const elements = [$titles, $texts];
      const classes = animate ? `${css.active} ${css.textAnimationFinished}` : css.active;
      const fadeInTimeout = animate ? 0.65 : 0;

      elements.forEach($element => $element.removeClass(classes));
      Animation.delay(fadeInTimeout, () => {
        elements.forEach($element => {
          $element.filter(`[data-related-slide-id=${slideIndex}]`).addClass(classes);
        });
      });
    };
    const stopAllVideoAndPlayCurrent = () => {
      const $videos = Array.from(this.$mainSlider.find('video'));
      const $currentSlide = $('.main_screen_slider__slide.slick-current');
      const $currentVideo = $currentSlide.find('video').get(0);

      $videos.forEach(video => video.pause());
      $currentVideo.play();
    };

    // set event listeners
    this.$mainSlider.on('init', () => {
      // Fix slider. Prevent slide change when clicked on 'time' element
      $('.slider_custom__time').on('click tap', e => e.stopPropagation());

      // Set active title / text
      setActiveTitleAndText(initialSlide, false);

      // play video
      stopAllVideoAndPlayCurrent();

      this._setTitleAnimatedLinesPosition(initialSlide);
    });

    this.$mainSlider.on('beforeChange', (e, slick, currSlide, nextSlide) => {
      // Set active title / text
      setActiveTitleAndText(++nextSlide, true);
    });

    this.$mainSlider.on('afterChange', stopAllVideoAndPlayCurrent);

    // initialize slider
    initializeSlider(this.$mainSlider, {
      prevArrow: $prevArrow,
      nextArrow: $nextArrow,
      initialSlide: initialSlide - 1,
      dotsClass: 'slider_custom__dots slider_custom__dots-time',
      cssEase: 'cubic-bezier(.74,.1,.32,.98)',
      speed: 1300,
      draggable: false,
      swipe: false,
      dots: true,
      slide: '.main_screen_slider__slide',
      customPaging: (slick, index) => {
        const time = $(slick.$slides[index]).data('time');

        return `<div class='slider_custom__dot_container'>
                    <span class='slider_custom__time is-hidden-for-animation'>${time}</span>
                    <span class='slider_custom__dot'></span>
                </div>`;
      }
    });

    return this;
  }

  /**
   * Initialize hot articles slider.
   *
   * @returns {MainScreen}
   */
  initHotSlider() {
    const $hotSliderTitles = this.$hotSlider.parent().find('.main_screen_hot__article_title');

    // set active title on slider change
    this.$hotSlider.on('beforeChange', (e, slick, currentSlide, nextSlide) => {
      $hotSliderTitles.removeClass(css.active);

      $hotSliderTitles
        .filter(`[data-related-slide-id='${nextSlide + 1}']`).addClass(css.active);
    });

    // initialize slider
    initializeSlider(this.$hotSlider, {
      dotsClass: 'slider_custom__dots slider_custom__dots-articles main_screen_hot__hidden',
      speed: 700,
      fade: true,
      cssEase: 'ease-in-out',
      arrows: false,
      dots: true,
      autoplay: true,
      autoplaySpeed: 2000,
      draggable: false,
      slide: '.main_screen_hot__article',
      customPaging: () => '',
      responsive: [
        {
          breakpoint: 767,
          settings: {
            arrows: true,
            prevArrow: this.$hotSlider.find('.slider_custom__prev'),
            nextArrow: this.$hotSlider.find('.slider_custom__next')
          }
        }
      ]
    });

    return this;
  }

  /**
   * Bind 'scroll down' button.
   *
   * @returns {MainScreen}
   */
  bindScrollDownButton() {
    this.$scrollDownButton.on('click tap', () => {
      const sectionHeight = this.$mainScreen.height() + 210;

      $scrolledElements.animate({ scrollTop: sectionHeight }, 1000, 'swing');
    });

    return this;
  }

  /**
   * Initialize home page's first section scripts.
   */
  init() {
    this
      .initMainSlider()
      .initHotSlider()
      .bindScrollDownButton();
  };
}

/**
 * Main page's home screen section instance.
 *
 * @constant
 * @type {MainScreen}
 */
export const Main01 = new MainScreen();
