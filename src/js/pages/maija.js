'use strict';

/**
 * 'Maija' pages scripts.
 *
 * @module Maija
 */

import {
    css,
    Resp,
    initializeSlider,
    bindMethods
} from '../modules/dev/_helpers';
import AjaxPageChange from '../components/ajaxPageChange';
import Animation from '../modules/dev/Animation';
import Common from '../components/common';

export default class Maija {

    constructor() {
        this.$mainContent = $('.about_content_wrapper');
        this.pageChangesInited = false;

        // save context
        bindMethods.bind(this)('init');

        // initialize after construction
        this.init();
    }

    /**
     * Change content, active tab, page and url on tab click.
     *
     * @return {Maija}
     */
    initPagesChange() {
        if (this.pageChangesInited) return this;

        const $tabs = $('.main_screen__tabs-tab');
        const {$mainContent, init} = this;

        AjaxPageChange.init({
            $eventHandler: $tabs,
            $animationTarget: $mainContent,
            afterChange: () => {
                Common.SR.sync();
                Common.initPopups();
                init();
            }
        });

        this.pageChangesInited = true;

        return this;
    }

    /**
     * Move panorama photo on 'mousemove' (desktop).
     * Swipe photo inside container (device).
     *
     * @return {Maija}
     */
    async initPanorama() {
        const isDevice = Resp.isTouch && !Resp.isDesk;
        const $panoramaContainer = this.$mainContent.find('.careers__photo');

        // no panorama photo found
        if (!$panoramaContainer.length) return this;

        // device
        if (isDevice) {
            $panoramaContainer.perfectScrollbar({
                suppressScrollY: true
            });

            return this;
        }

        // desktop
        await new Promise(resolve => {
            imagesLoaded($panoramaContainer, resolve);
        });

        const $panoramaPhoto = $panoramaContainer.find('img');
        const windowWidth = Resp.currWidth;
        const containerWidth = $panoramaContainer.width();
        const photoWidth = $panoramaPhoto.width();
        const windowContainerWidthDiff = (windowWidth - containerWidth) / 2;
        const minX = -(photoWidth - containerWidth);
        const maxX = 0;

        // set initial position
        const initialCords = (photoWidth - containerWidth) / 2;
        Animation.setTween($panoramaPhoto, { x: -initialCords });

        // animate on mousemove
        $panoramaContainer.on('mousemove', e => {
            let x = -(e.pageX - windowContainerWidthDiff - windowWidth / 11);

            x = (x / containerWidth) * photoWidth;

            x = x < minX ? minX
                : x > maxX ? maxX
                    : x;

            Animation.tweenWithTimeout($panoramaPhoto, 0, {
                time: 8,
                params: {ease: Power0.easeNone, css: {x}}
            });
        });
   
       $panoramaContainer.on('click', e => {
          Animation.kill($panoramaPhoto, false);
       });

        return this;
    }

    /**
     * Initialize 'client service' sliders.
     *
     * @return {Maija}
     */
    initServicesSliders() {
        const $serviceSliders = $('.client_service__slider-slides');

        if (!$serviceSliders.length) return this;

        $serviceSliders.each((index, el) => {
            const $slider = $(el);
            const $container = $slider.parent();
            const $labels = $container.find('.client_service__slider-label');
            const $prevArrow = $container.find('.slider_custom__prev');
            const $nextArrow = $container.find('.slider_custom__next');

            // set active label on slider change
            $slider.on('beforeChange', (e, slick, currentSlide, nextSlide) => {
                $labels.removeClass(css.active);

                $labels
                    .filter(`[data-related-slide-id='${nextSlide + 1}']`).addClass(css.active);
            });

            // initialize slick slider
            initializeSlider($slider, {
                prevArrow: $prevArrow,
                nextArrow: $nextArrow,
                cssEase: 'cubic-bezier(.74,.1,.32,.98)',
                speed: 1300,
                slide: 'img',
                draggable: false,
                dots: false
            });
        });

        return this;
    }

    /**
     * Initialize slider table (devices only).
     *
     * @return {Maija}
     */
    initTableSlider() {
        const $sliderTable = $('.slider-table');
        const $sliderTableParent = $sliderTable.parent();

        if (Resp.isDesk || !$sliderTable.length) return this;

        const $slider = $sliderTable.find('.slider-table__body');
        const $prevArrow = $sliderTableParent.find('.slider_custom__prev');
        const $nextArrow = $sliderTableParent.find('.slider_custom__next');

        initializeSlider($slider, {
            prevArrow: $prevArrow,
            nextArrow: $nextArrow,
            cssEase: 'cubic-bezier(.74,.1,.32,.98)',
            speed: 1000,
            slide: '.slider-table__row',
            draggable: false,
            swipe: false,
            dots: false,
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });

        return this;
    }

    /**
     * Initialize 'Maija' pages scripts.
     *
     * @return {Maija}
     */
    init() {
        return this
            .initPagesChange()
            .initServicesSliders()
            .initTableSlider()
            .initPanorama();
    }
}
