'use strict';

/**
 * Gallery pages scripts.
 *
 * @module Gallery
 */

import {initializeSlider, css, $body, $document} from '../modules/dev/_helpers';
import News from './news';
import Animation from '../modules/dev/Animation';
import 'vintage-popup';

export default class Gallery extends News {

    constructor() {
        super();

        this.$sliderContainer = $('.main_screen__container');
        this.$slider = $('.main_screen__slider');

        this.$labelsContainer = $('.main_screen__slider-labels');
        this.$labels = $('.main_screen__slider-label');
        this.$title = $('.main_screen__title');

        // initialize slider
        this.initSlider();
        this.initImgScale();
        this.initVideoPlay();
    }

   /**
   * Init playing video.
   *
   * @return {Gallery}
   */
   initVideoPlay() {
      const video = $('.main_screen__container_video').find('video')[0];
      
      $document.on('click tap', '.main_screen__container_video', function () {
         
         $(this).toggleClass('video-played');
         // video.paused ? video.play() : video.pause();
         
         
         if (video.paused) {
            video.play();
            video.setAttribute("controls","controls");
         } else {
            video.pause();
            video.removeAttribute("controls");
         }
   
         video.onplay = function() {
            video.setAttribute("controls","controls");
            $('.main_screen__container_video').addClass('video-played');
         };
         video.onpause = function() {
            video.removeAttribute("controls");
            $('.main_screen__container_video').removeClass('video-played');
         };
      });
   }

    /**
     * Initialize intro section slider (single gallery item page).
     *
     * @return {Gallery}
     */
    initSlider() {
        // no slider found
        if (!this.$slider.length) return this;

        const $container = this.$sliderContainer;
        const $labelsContainer = this.$labelsContainer;
        const $labels = this.$labels;
        const $prevArrow = $container.find('.slider_custom__prev');
        const $nextArrow = $container.find('.slider_custom__next');
        const $paging = $container.find('.main_screen__slider-count');
        const $current = $paging.find('.current');
        const $total = $paging.find('.total');
        const roundNumber = number => number >= 10 ? number : `0${number}`;
        const setActiveLabel = index => {
            const $activeLabel = $labels.filter(`[data-related-slide-id="${index}"]`);
            $labels.removeClass(css.active);
            $activeLabel.addClass(css.active);
            Animation.tweenWithTimeout($labelsContainer, 0, {
               time: 0.7,
               params: {
                   ease: Power2.easeInOut,
                   height: $activeLabel.prop('scrollHeight')
               }
            });
        };

        // bind event listeners
        this.$slider.on('init', (e, slick) => {
            $total.text(roundNumber(slick.slideCount));

            if ($labelsContainer) $labelsContainer.css({ height: $labels.active().prop('scrollHeight') });
        });
        this.$slider.on('beforeChange', (e, slick, currSlide, nextSlide) => {
            const id = $(slick.$slides.get(nextSlide)).data('slide-id');

            if ($labelsContainer) setActiveLabel(id);
            if(this.$title) {
                id != 0 ? this.$title.fadeOut(1300) : this.$title.fadeIn(1300)
            }
            $current.text(roundNumber(++nextSlide));
        });

        // initialize slider
        initializeSlider(this.$slider, {
            prevArrow: $prevArrow,
            nextArrow: $nextArrow,
            cssEase: 'cubic-bezier(.74,.1,.32,.98)',
            speed: 1300,
            draggable: false,
            swipe: false,
            slide: '.main_screen__slider-item',
            slidesToShow: 1,
            centerMode: true,
            variableWidth: true,
            infinite: true
        });

        return this;
    }

    /**
    * Init IMG scale on gallery:
    */
   initImgScale() {
      let $img = $('.main_screen__slider-item > img');

        if ($img.length) {
          // Set popup target for this img:
         $img.each(function (index, element) {
            $(this).attr("data-popup-target", "img");
         });
         
         // Append popup on body(if img to show is exist):
         $body.append('<div class="popup popup_img" data-popup-id="img" tabindex="-1" role="dialog"><div class="popup__container"><img src=""><div class="popup__close"><span></span><span></span></div><a title="Загрузить фото" class="popup__download" href="" download><svg height="24px" width="24px" version="1.1" viewBox="0 0 26 26" xmlns="http://www.w3.org/2000/svg"><title/><desc/><defs/><g fill="none" fill-rule="evenodd" id="miu" stroke="none" stroke-width="1"><g transform="translate(-827.000000, -299.000000)"><g id="slice" transform="translate(215.000000, 119.000000)"/><path d="M838.00265,318.173724 L835.464466,315.63554 L834.050253,317.049754 L839,321.999501 L843.949747,317.049754 L842.535534,315.63554 L840.001088,318.169986 L840.001088,307 L838.00265,307 L838.00265,318.173724 Z M835,313.997662 L832.674206,313.997131 L831.973178,313.940988 C829.708243,313.593583 828,311.764626 828,309.572042 C828,308.137521 828.734966,306.836489 829.912985,306.019173 C829.628479,303.850891 831.460766,302.286258 833.375,302.286258 C833.938771,302.286258 834.483332,302.412496 834.969711,302.646025 C835.729675,301.504761 837.910341,300 840.5625,300 C844.09345,300 847.040554,302.452518 847.450789,305.660079 C848.56012,306.019173 850,307.657641 850,309.572042 C850,311.764626 848.291757,313.593583 845.950794,313.949691 L845.247324,314 L843,313.999487 L843,311.999498 L845.174206,312 L845.723178,311.961297 C847.045116,311.758706 848,310.737221 848,309.5684 C848,308.552512 847.278783,307.636759 846.19121,307.286053 L845.511567,307.066891 L845.498907,306.354374 C845.45637,303.960374 843.277369,302 840.5625,302 C838.638388,302 836.925929,302.995106 836.11128,304.510772 L835.470936,305.702142 L834.518638,304.740479 C834.235808,304.454867 833.824346,304.284298 833.375,304.284298 C832.527984,304.284298 831.875,304.880942 831.875,305.570879 C831.875,305.742025 831.913616,305.907243 831.988558,306.062017 L832.434324,306.982638 L831.500972,307.404741 C830.579121,307.821643 830,308.656789 830,309.5684 C830,310.737221 830.954884,311.758706 832.200794,311.952601 L832.75,311.99713 L835,311.99765 L835,313.997662 L835,313.997662 Z" fill="#fff"/></g></g></svg></a></div></div>');
         
         // Init popup:
         $img.popup();
   
         $img.on("click tap", function () {
            $('[data-popup-id = "img"] .popup__download').attr("href", $(this).attr("src"));
            $('[data-popup-id = "img"] img').attr("src", $(this).attr("src"));
         });
      }
      
      return this;
   }
};
