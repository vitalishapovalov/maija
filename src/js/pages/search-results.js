'use strict';

/**
 * Search results pages scripts.
 *
 * @module SearchResults
 */

import News from './news';

export default class SearchResults extends News {};
