'use strict';

/**
 * 'Services' pages scripts.
 *
 * @module Services
 */

import Tabs from '../components/tabs';
import Animation from '../modules/dev/Animation';
import { css, Resp } from '../modules/dev/_helpers';

export default class Services {

  constructor() {
    this.$tabs = $('.salons__tabs');

    // initialize after construction
    this.init();
  }

  /**
   * Initialize 'Services' pages scripts.
   *
   * @return {Services}
   */
  init() {
    return this.initTabs();
  }

  /**
   * Initialize tabs (tabs section).
   *
   * @return {Services}
   */
  initTabs() {
    new Tabs(this.$tabs);

    return this;
  }
}

/**
 * 'Single service' pages scripts.
 *
 * @module Service
 */

export class Service {
  constructor() {
    this.$servicesContainer = $('.main_screen__services-container');
    this.$logosContainer = $('.logos__container');
    this.$logosToggleButton = $('.logos__button');

    // initialize after construction
    this.init();
  }

  /**
   * Initialize perfect-scrollbar on 'services' block (main screen).
   *
   * @return {Service}
   */
  initServicesScrollbar() {
    if (Resp.isMobile) return this;

    this.$servicesContainer.perfectScrollbar({
      suppressScrollX: true,
      swipePropagation: false,
      wheelSpeed: 0.41
    });

    return this;
  }

  /**
   * Show / hide all logos.
   *
   * @return {Service}
   */
  async initLogosToggle() {
    const $container = this.$logosContainer;
    const $items = $container.children();

    // not enough items for toggle
    if ($items.length < 8) return this;

    // preloader images
    await new Promise(resolve => {
      imagesLoaded($container, resolve);
    });

    const $button = this.$logosToggleButton;
    const $buttonText = $button.find('.btn__text');
    const containerHeight = $container.prop('scrollHeight');
    const active = css.active;
    const initialText = $buttonText.text();
    const toggleText = $button.data('opened-state-text');
    let initialHeight = $container.height();
    const animateContainersHeight = height =>
      Animation.tweenWithTimeout($container, 0, {
        time: 1,
        params: { ease: Power3.easeOut, css: { height } }
      });

    // set initial height (mobile only)
    if (Resp.isMobile) {
      const height = initialHeight = (function () {
        const $firstThreeItems = $items.slice(0, 3);
        const $fourthItem = $items.slice(3, 4);
        const fourthItemHeight = $fourthItem.outerHeight();
        const firstThreeItemsHeight = (function () {
          let totalHeight = 0;
          $firstThreeItems.each((index, el) => {
            totalHeight += $(el).outerHeight(true);
          });
          return totalHeight;
        })();

        return firstThreeItemsHeight + fourthItemHeight;
      })();

      $container.css({ height });
    }

    // init container full size toggle on click
    $button.on('click tap', () => {
      const isOpened = $container.hasClass(active);

      if (!isOpened) {
        $container.addClass(active);
        $buttonText.text(toggleText);
        animateContainersHeight(containerHeight);

      } else {
        $container.removeClass(active);
        $buttonText.text(initialText);
        animateContainersHeight(initialHeight);
      }
    });

    return this;
  }

  /**
   * Initialize 'Single service' pages scripts.
   *
   * @return {Service}
   */
  init() {
    return this
      .initServicesScrollbar()
      .initLogosToggle();
  }
}
