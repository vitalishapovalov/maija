'use strict';

/**
 * News and actions pages scripts.
 *
 * @module News
 */

import {
  $scrolledElements,
  $window,
  $document,
  css,
  _csrf
} from '../modules/dev/_helpers';
import AjaxTabs from '../components/ajaxTabs';
import Animation from '../modules/dev/Animation';

export default class News {

  constructor() {
    this.$tabs = $('.c_tabs--ajax');
    this.$otherNews = $('.other-news');

    // initialize after construction
    this.init();
  }

  /**
   * Initialize Tabs.
   *
   * @return {News}
   */
  initTabs() {
    // no tabs found
    if (!this.$tabs.length) return this;

    // create tab instance
    this.Tabs = new AjaxTabs(this.$tabs);

    return this;
  }

  /**
   * Initialize 'other news' section content upload.
   *
   * @return {News}
   */
  initOtherNews() {
    // no 'other news' found
    if (!this.$otherNews.length) return this;

    const $button = this.$otherNews.find('.btn');
    const $btnDots = $button.find('.btn__dots');
    const $container = this.$otherNews.find('.other-news__content');

    const url = $button.data('content-url');
    const current = $button.data('current');

    const launchPreloader = () => {
      $btnDots.css({ transition: 'none' });
      Animation.tweenWithTimeout($btnDots, 0, {
        time: 1,
        params: { rotation: 360, repeat: -1, ease: Power0.easeNone }
      });
    };
    const stopPreloader = () =>
      Animation.kill($btnDots, ['transform', 'transition']);

    // append new content
    function processResponse(response) {
      const $content = $(response);
      const newsCount = $content.filter('a').length;
      const currentScrollTop = $window.scrollTop();

      // add to DOM
      $container.append($content);

      // preload images
      imagesLoaded($container, () => {

        // scroll to the uploaded content
        $scrolledElements.scrollTop(currentScrollTop);

        // stop preloader
        stopPreloader();
      });

      // no content left on server
      if (newsCount < 3) $button.remove();
    }

    // upload and append new cyontent
    function uploadContent() {
      const newsCount = $container.children().length;

      $.ajax({
        url,
        data: { _csrf, current, newsCount },
        method: 'post',
        dataType: 'html',
        beforeSend: launchPreloader,
        error: stopPreloader,
        success: processResponse
      });
    }

    // upload content on button's click
    $button.on('click tap', uploadContent);

    return this;
  }

  // /**
  //  * Initialize video-items.
  //  *
  //  * @return {News}
  //  */
  // initVideoItems() {
  //   const $videoContainer = $('.video_block__video');
  //   const $closeButton = $videoContainer.find('.video_block__video_close');
  //   const $video = $videoContainer.find('video');
  //   const video = $video[0];
  //   const $videoSource = $video.find('source');
  //
  //   $document.on('click tap', '.c_tabs__ajax-item--5', function () {
  //     const $this = $(this);
  //     const videoSrc = $this.data('video-src');
  //
  //     // open video
  //     $videoContainer.addClass(css.active);
  //
  //     $videoSource.attr('src', videoSrc);
  //
  //     // load and play
  //     video.load();
  //     video.play();
  //   });
  //
  //   // close on 'X' button click
  //   $closeButton.on('click tap', closeMenu.bind(this));
  //
  //   // close on 'esc' press
  //   $document.on('keyup', e => {
  //     if (e.keyCode === 27 && $videoContainer.hasClass(css.active)) closeMenu();
  //   });
  //
  //   // pause video and close it
  //   function closeMenu() {
  //     // pause video
  //     video.pause();
  //
  //     // close video
  //     $videoContainer.removeClass(css.active);
  //   }
  //
  //   return this;
  // }

  /**
   * Initialize 'News and actions' pages scripts.
   *
   * @return {News}
   */
  init() {
    return this
      .initTabs()
      .initOtherNews()
      // .initVideoItems();
  }
}
