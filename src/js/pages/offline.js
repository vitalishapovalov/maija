'use strict';

/**
 * Offline page scripts.
 *
 * @module Offline
 */

export default class Offline {

  constructor() {
    this.timeout = 5000;

    this.init();
  }

  /**
   * @this Offline
   * @return {Promise.<*>}
   */
  init = () => Promise.resolve()
      .then(this.checkInternetConnection)
      .then(this.registerReloadWhenOnline);

  /**
   * @async
   * @this Offline
   * @return {Promise.<Boolean>}
   */
  checkInternetConnection = async () => {
    let connection;

    try {
      connection = await new Promise((resolve, reject) => {
        $.ajax({
          url: `${window.location.origin}/static/favicon.png`,
          success: resolve,
          error({ status }) {
            if (status === 404) {
              return resolve(true);
            }
            reject();
          }
        });
      });
    } catch (err) {
      connection = false;
    }

    if (connection) {
      console.log(
        '%cYour device is connected to the internet. Apparently, you got here by mistake. Redirecting in 5 sec...',
        'font-size: 24px; color: red; background-color: yellow;'
      );
    }

    return connection;
  };

  /**
   * @this Offline
   * @param {Boolean} connection
   */
  registerReloadWhenOnline = (connection) => {
    const { timeout, init } = this;

    if (connection) {
      return setTimeout(() => { window.location.href = window.location.origin; }, timeout);
    }

    setTimeout(init, timeout);
  };
}
