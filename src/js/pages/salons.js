'use strict';

/**
 * Salons pages scripts.
 *
 * @module Salons
 */

import { initializeSlider, Resp } from '../modules/dev/_helpers';
import Gallery from './gallery';

export default class Salons {

  constructor() {
    this.$sliderContainer = $('.main_screen__container');
    this.$slider = $('.main_screen__slider');
    this.initSlider = Gallery.prototype.initSlider.bind(this);

    this.$teamSlider = $('.team__section');

    this.mapContainer = document.getElementById('map');
    this.callbackName = 'runGmaps';

    // initialize after construction
    this.init();
  }

  /**
   * Initialize 'Salons' page scripts.
   *
   * @return {Salons}
   */
  init() {
    return this
      .initSlider()
      .initTeamSlider()
      .setGoogleMapsSettings()
      .initGoogleMaps();
  }

  /**
   * Initialize slider in 'Team' section (mobile only).
   *
   * @return {Salons}
   */
  initTeamSlider() {
    // only mobile devices / no sliders found
    if (!Resp.isMobile || !this.$teamSlider.length) return this;

    // initialize each slider
    this.$teamSlider.each((i, el) => {
      const $sliderContainer = $(el);
      const $slider = $sliderContainer.find('.team__section_container');
      const $prevArrow = $sliderContainer.find('.slider_custom__prev');
      const $nextArrow = $sliderContainer.find('.slider_custom__next');

      initializeSlider($slider, {
        prevArrow: $prevArrow,
        nextArrow: $nextArrow,
        speed: 300,
        draggable: true,
        slide: '.team__item',
        slidesToShow: 1,
        adaptiveHeight: true,
        infinite: true
      });
    });

    return this;
  }

  /**
   * Set google maps pin icon and map styles.
   *
   * @return {Salons}
   */
  setGoogleMapsSettings() {
    this.icon = '/static/img/world-map-pin.png';

    // jscs:disable
    this.styles = [
      {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#d3d3d3"
          }
        ]
      },
      {
        "featureType": "transit",
        "stylers": [
          {
            "color": "#808080"
          },
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "visibility": "on"
          },
          {
            "color": "#b3b3b3"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#ffffff"
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "visibility": "on"
          },
          {
            "color": "#ffffff"
          },
          {
            "weight": 1.8
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "color": "#d7d7d7"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "visibility": "on"
          },
          {
            "color": "#ebebeb"
          }
        ]
      },
      {
        "featureType": "administrative",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#a7a7a7"
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#ffffff"
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#ffffff"
          }
        ]
      },
      {
        "featureType": "landscape",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "visibility": "on"
          },
          {
            "color": "#efefef"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#696969"
          }
        ]
      },
      {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "visibility": "on"
          },
          {
            "color": "#737373"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels.icon",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "color": "#d6d6d6"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {},
      {
        "featureType": "poi",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#dadada"
          }
        ]
      }
    ];
    // jscs:enable

    return this;
  }

  /**
   * Initialize google maps.
   *
   * @return {Salons}
   */
  initGoogleMaps() {
    // no map found
    if (!this.mapContainer) return this;

    const { styles, icon, mapContainer, callbackName } = this;
    const { url, markers, center, zoom } = global.gmaps;
    const $head = $('head');
    const script = `
        <script 
            type = 'text/javascript'
            charset = 'UTF-8'
            src = ${url}&callback=${callbackName}>
        </script>`;

    // load gmaps
    $head.append(script);

    // gmaps initialization
    global[callbackName] = function () {

      // create maps instance
      const map = new google.maps.Map(mapContainer, {
        center, zoom, styles,
        scrollwheel: false
      });

      // add markers
      markers.forEach(markerData => {
        const { lat, lng, title, desc: content } = markerData;
        const infowindow = new google.maps.InfoWindow({ content });
        const marker = new google.maps.Marker({
          map, title, icon,
          position: new google.maps.LatLng(lat, lng)
        });

        google.maps.event.addListener(marker, 'click', function () {
          infowindow.open(map, marker);
        });
      });
    };

    return this;
  }
};
