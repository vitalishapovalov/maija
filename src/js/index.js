'use strict';

/**
 * App entry point.
 *
 * @module App
 */

/** Import commonly used libs. Initialized by default */
import 'jquery';
import 'babel-polyfill';
import 'picturefill';
import 'gsap';
import './modules/dep/CustomEase';
import 'perfect-scrollbar/jquery';
import 'slick-carousel';
import './components/common';

/** Import page controllers */
import Home from './pages/home/home';
import Maija from './pages/maija';
import Services, { Service } from './pages/services';
import News from './pages/news';
import Gallery from './pages/gallery';
import Salons from './pages/salons';
import SearchResults from './pages/search-results';
import Offline from './pages/offline';

/** Import utils */
import { currentPage } from './modules/dev/_helpers';

/**
 * Run appropriate scripts for each page.
 **/
switch (currentPage) {
  /** Home page */
  case 'home'    : new Home;     break;

  /** 'Maija' pages (About us, Why us) */
  case 'maija'   : new Maija;    break;

  /** Services page */
  case 'services': new Services; break;

  /** Single service page */
  case 'service' : new Service;  break;

  /** Actions, news pages, single article */
  case 'news'    : new News;     break;

  /** Gallery pages, single gallery item */
  case 'gallery' : new Gallery;  break;

  /** Salons pages, single salons page */
  case 'salons'  : new Salons;   break;

  /** Offline page */
  case 'offline' : new Offline;  break;

  /** Search results pages */
  case 'search-results': new SearchResults; break;

  /** No pages found */
  default: break;
}
