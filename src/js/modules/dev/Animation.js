/**
 * Animations handler.
 *
 * @module Animation
 */

import { $window, $body } from './_helpers';

export default class Animation {
  /**
   * Checks tween target for errors.
   *
   * @param {jQuery|HTMLElement|String|Array} target
   * @returns {Boolean}
   */
  static checkForErrors(target) {
    if (Array.isArray(target)) {
      if (!target.length || typeof target[0] === 'undefined' || target[0] === null) {
        console.warn('Tween target not found');
        return false;
      }
    } else if (!target) {
      console.warn('Tween target not found');
      return false;
    }

    return true;
  }

  /**
   * Set css-props without animation.
   *
   * @param {jQuery|HTMLElement|String|Array} target
   * @param {Object} settings
   * @returns {Animation}
   */
  static setTween(target, settings) {
    if (this.checkForErrors(target)) TweenMax.set(target, settings);

    return this;
  }

  /**
   * Delay function's call.
   *
   * @param {Number} timeout
   * @param {Function} func
   * @returns {Animation}
   */
  static delay(timeout, func) {
    TweenMax.delayedCall(timeout, func);

    return this;
  }

  /**
   * Kill target's animations (tweens).
   * Remove css property (if specified).
   *
   * @param {jQuery|HTMLElement|String|Array} target
   * @param {Array} [properties]
   * @return {Animation}
   */
  static kill(target, properties) {
    TweenMax.killTweensOf(target);

    if (properties) properties.forEach(prop => $(target).css(prop, ''));

    return this;
  }

  /**
   * Delay function's call on multiple items.
   *
   * @param {jQuery} $items - items
   * @param {Function} func - this passed as argument on each call
   * @param {Number} stagger - stagger time in seconds
   * @param {Number} timeout - timeout before animation start
   * @returns {Animation}
   */
  static staggerDelay($items, stagger, func, timeout) {
    let timeOut = 0;

    if (!$items) {
      console.warn('staggerDelay target was not found');

      return this;
    }

    this.delay(timeout, () => {
      $items.each((index, element) => {
        Animation.delay(timeOut, () => func.call(null, element, index));
        timeOut += stagger;
      });
    });

    return this;
  }

  /**
   * Perform tween animation with timeout.
   *
   * @param {jQuery|HTMLElement|String|Array} target
   * @param {Number} timeout
   * @param {Object} settings
   * @param {String} [method = 'to'] - TweenMax method used for animation (doesn't support fromTo)
   * @returns {Promise}
   */
  static tweenWithTimeout(target, timeout, settings, method = 'to') {
    const _this = this;

    return new Promise(resolve => {
      if (!_this.checkForErrors(target)) return;

      if (!settings.params.onComplete) settings.params.onComplete = resolve;

      _this.delay(timeout, () => {
        if (method == 'staggerFrom' || method == 'staggerTo') {
          TweenMax[method](target, settings.time, settings.params, settings.stagger);
        } else {
          TweenMax[method](target, settings.time, settings.params);
        }
      });
    });
  }

  /**
   * Stepped animation maximum of 5 steps.
   * Each step is a promise that must resolve on animation end.
   *
   * @param {Object} params - object with functions (animation steps)
   * @param {Function} params.one
   * @return {Promise}
   */
  static async steppedAnimation(params) {
    const steps = Array.from(Object.values(params));
    const [firstSTep] = steps;

    // first step required
    if (!firstSTep) {
      return Promise.resolve(console.error('First step required'));
    }

    // first step must be a function
    if (!(typeof firstSTep === 'function')) {
      return Promise.resolve(console.error('Step must be a function'));
    }

    // steps animation
    const stepsAnimation = steps.map(async step => await step());

    // run steps
    return Promise.all(stepsAnimation);
  }

  /**
   * Run preloader's animation.
   *
   * @param {Object} params - animation settings
   * @param {Function} callback - on animation complete
   * @param {Function} asyncProcedure
   * @returns {Promise.<Animation>}
   */
  static async runPreloader(params, asyncProcedure, callback) {
    const logoParams = params.logo;
    const curtainsParams = params.curtains;

    const animationPartOne = () => new Promise(resolve => {
      // animate left block lines
      TweenMax.to(logoParams.$leftBlockLines, logoParams.time * 5, {
        ease: Sine.easeOut,
        css: { x: '-=0%' }
      });

      // animate right block line
      TweenMax.to(logoParams.$rightBlockLines, logoParams.time * 5, {
        ease: Sine.easeOut,
        css: { x: '+=0%' }
      });

      $window.on('load', () => {
        setTimeout(() => {
          // show logo with delay
          TweenMax.to(logoParams.$logo, 0, {
            delay: 1,
            css: { opacity: 1 }
          });

          TweenMax.to(logoParams.$leftBlockLines, 2, {
            ease: logoParams.ease,
            css: { x: '-=100%' },
            onComplete: resolve
          });
          TweenMax.to(logoParams.$rightBlockLines, 2, {
            ease: logoParams.ease,
            css: { x: '+=100%' }
          });
        }, 1000);
      });
    });

    await asyncProcedure(animationPartOne);

    // show left curtains
    TweenMax.to([curtainsParams.$leftCurtain, curtainsParams.$rightCurtain], curtainsParams.time, {
      ease: curtainsParams.ease,
      css: { x: '0%' },
      onComplete: callback
    });

    return this;
  }

  /**
   * Open menu animation.
   *
   * @param {Object} params - animations settings
   * @param {Function} beforeOpen
   * @param {Function} afterOpen
   * @returns {Animation}
   */
  static openMenu(params, beforeOpen, afterOpen) {
    typeof beforeOpen === 'function' ? beforeOpen() : null;

    // burger transformation
    TweenMax.to([params.$burgerLine1, params.$burgerLine4], params.time, {
      ease: params.ease,
      css: { width: '0%', left: '50%', top: '7px' }
    });
    TweenMax.to(params.$burgerLine2, params.time, {
      ease: params.ease,
      css: { transform: 'rotate(45deg)' }
    });
    TweenMax.to(params.$burgerLine3, params.time, {
      ease: params.ease,
      css: { transform: 'rotate(-45deg)' }
    });

    // show menu itself
    TweenMax.to([params.$menuInner, params.$menu], params.time, {
      ease: params.ease,
      css: { opacity: 1, y: '0px', visibility: 'visible' },
      onComplete: afterOpen
    });

    return this;
  }

  /**
   * Close menu animation.
   *
   * @param {Object} params - animations settings
   * @param {Function} beforeClose
   * @param {Function} afterClose
   * @returns {Animation}
   */
  static closeMenu(params, beforeClose, afterClose) {
    typeof beforeClose === 'function' ? beforeClose() : null;

    // burger transformation
    TweenMax.to(params.$burgerLine1, params.time, {
      ease: params.ease,
      css: { width: '100%', left: '0%', top: '0px' }
    });
    TweenMax.to(params.$burgerLine4, params.time, {
      ease: params.ease,
      css: { width: '100%', left: '0%', top: '14px' }
    });
    TweenMax.to([params.$burgerLine2, params.$burgerLine3], params.time, {
      ease: params.ease,
      css: { transform: 'rotate(0deg)' }
    });

    // hide menu
    TweenMax.to(params.$menu, params.time, {
      ease: params.ease,
      css: { opacity: 0 }
    });
    TweenMax.to(params.$menuInner, params.time, {
      ease: params.ease,
      css: { y: '-25px' },
      onComplete: afterClose
    });

    return this;
  }

  /**
   * Run header's reveal animation.
   *
   * @param {Object} params - animation settings
   * @returns {Animation}
   */
  static revealHeader(params) {
    // reveal header
    TweenMax.to(params.$rightLine, params.time.line1, {
      ease: params.ease.line1,
      css: { y: '0%' },
      onComplete: () => {
        TweenMax.to([params.$leftLine, params.$rightLine], params.time.line2, {
          delay: params.linesDelay,
          ease: params.ease.line2,
          css: { x: '0%' }
        });
      }
    });

    // show logo, button and burger
    this.tweenWithTimeout([params.$button, params.$logo, params.$burger], params.componentsTimeout, {
      time: params.time.components,
      params: {
        ease: params.ease.components,
        css: { y: '0px' },
        onComplete: () => params.$button.css('transform', 'none')
      }
    });

    // show links
    this.staggerDelay(params.$links, params.linksStaggerDelay, (element) => {
      $(element).addClass('linksAnimationFinished');
    }, params.linksTimeout);

    return this;
  }

  /**
   * Change dots line position.
   *
   * @param {Object} settings - animation settings
   * @returns {Animation}
   */
  static changeDotLinePosition(settings) {
    const css = { x: settings.x };

    if (settings.width) css.width = settings.width;

    // move line to the active dot
    TweenMax.to(settings.$element, settings.time, {
      ease: settings.ease, css
    });

    return this;
  }

  /**
   * Run line animation. Y-axis, top to bot.
   *
   * @param {jQuery|Array} lines
   * @param {Object} [params]
   * @param {Number} [timeout]
   * @returns {Animation}
   */
  static runLine(lines, params = {}, timeout = 0) {
    let arrayFlag = false;

    // check for array
    if (Array.isArray(lines)) {
      let realLines = [];

      lines.forEach(element => {
        const realLine = $(element).find('.anim_line__inner');
        realLines.push(realLine);
      });

      arrayFlag = true;
      lines = realLines;
    }

    // settings
    const time = params.time || 0.84;
    const ease = params.ease || Power2.easeInOut;
    const css = params.css || { y: '100%' };

    // run animation
    this.tweenWithTimeout(lines, timeout, {
      time,
      params: {
        ease,
        css,
        onComplete: () => {
          arrayFlag ? lines.forEach(element => $(element).parent().hide()) : null;
        }
      }
    });

    return this;
  }

  /**
   * Run reveal animation on specified item.
   * (add 'anim_finished' class)
   *
   * @param {jQuery|HTMLElement|String|Array} text
   * @param {Object} [params]
   * @param {Number} [timeout]
   * @param {String} [cssClassName='anim_finished']
   * @returns {Animation}
   */
  static revealTextBlock(text, params = {}, timeout = 0, cssClassName = 'anim_finished') {
    // run animation
    this.delay(timeout, () => text.addClass(cssClassName));

    return this;
  }

  /**
   * Run section's title reveal animation.
   *
   * @param {jQuery} $title
   * @param {Object} params
   * @param {Number} timeout
   * @returns {Animation}
   */
  static revealSectionTitle($title, params = {}, timeout = 0) {
    if (!$title) return this;

    // DOM elements
    const $text = params.dynamicText
      ? $title.find(`.anim_section_title__text.${params.dynamicText}`)
      : $title.find('.anim_section_title__text');
    const $leftLine = $title.find('.anim_section_title__line-left div');
    const $rightLine = $title.find('.anim_section_title__line-right div');

    // settings
    const lineTime = params.lineTime || 1.16;
    const lineEase = params.lineEase || Power4.easeInOut;

    // run animation
    this.delay(timeout, () => {
      // text
      Animation.revealTextBlock($text, {}, 0.52);

      // left line
      TweenMax.to($leftLine, lineTime, {
        ease: lineEase,
        css: { x: '100%' }
      });

      // right line
      TweenMax.to($rightLine, lineTime, {
        ease: lineEase,
        css: { x: '-100%' }
      });
    });

    return this;
  }

  /**
   * Run section's mark reveal animation.
   *
   * @param {jQuery} $sectionMark
   * @param {Number} [timeout = 0]
   * @returns {Animation}
   */
  static revealSectionMark($sectionMark, timeout = 0) {
    if (!$sectionMark) return this;

    // DOM elements
    const $numberContainer = $sectionMark.find('.section_mark__number_container');
    const $line = $sectionMark.find('.section_mark__line');
    const $text = $sectionMark.find('.section_mark__text');
    const $blackLine = $sectionMark.find('.anim_line__inner');

    // settings
    const numberTime = 0.2;
    const numberEase = Power2.ease;
    const lineTime = 0.48;
    const lineTimeout = 0.15;
    const lineEase = Power3.easeInOut;
    const textTimeout = 0.7;
    const blackLineTimeout = 0.3;

    // run animation
    this.delay(timeout, () => {
      // show section number
      TweenMax.to($numberContainer, numberTime, {
        ease: numberEase,
        css: { transform: 'scale(1)' }
      });

      // reveal line
      this.tweenWithTimeout($line, lineTimeout, {
        time: lineTime,
        params: {
          ease: lineEase,
          css: { height: '40px' }
        }
      });

      // animate black line
      Animation.runLine($blackLine, { css: { x: '102%' } }, blackLineTimeout);

      // show text
      this.tweenWithTimeout($text, textTimeout, {
        time: 0,
        params: { opacity: 1 }
      });
    });

    return this;
  }

  /**
   * Reveal video block.
   *
   * @param {Object} params - animation settings
   * @returns {Animation}
   */
  static revealVideo(params) {
    // show video
    this.delay(params.videoTimeout, () => {
      TweenMax.set(params.$videoContainer, { opacity: 1 });
      TweenMax.to(params.$videoLine, params.videoTime, {
        ease: params.videoEase,
        css: { x: '0' },
        onComplete: () => {
          TweenMax.set([
            '.back_stage .video_block__play',
            '.back_stage .video_block__img',
            '.back_stage .video_block__backdrop'
          ], { opacity: 1 });
          removeBlackLineFromVideo();
        }
      });
    });

    function removeBlackLineFromVideo() {
      TweenMax.to(params.$videoLine, params.videoTime, {
        ease: params.videoEase,
        css: { x: '100%' },
        onComplete: () => params.$videoLine.remove()
      });
      TweenMax.to('.back_stage .video_block__inner', 1, {
        boxShadow: '0px 0px 56px 16px #bdbbbd'
      });
    }

    return this;
  }

  /**
   * Reveal photos block (gallery).
   *
   * @param {jQuery} $photos
   * @param {Number} timeout
   * @returns {Animation}
   */
  static revealPhotos($photos, timeout = 0) {
    Animation.delay(timeout, () => $photos.removeClass('photos-hidden'));

    return this;
  }

  /**
   * Animate curtains (e.g. slide/tab change animation)
   *
   * @param {Object} params - animation settings
   * @returns {Animation}
   */
  static animateCurtains(params) {
    // left curtain
    TweenMax.fromTo(params.$leftCurtain, params.time,
      { css: { x: '100%' } },
      { ease: params.ease, css: { x: '-101%' } }
    );

    // right curtain
    TweenMax.fromTo(params.$rightCurtain, params.time,
      { css: { x: '-100%' } },
      { ease: params.ease, css: { x: '101%' } }
    );

    return this;
  }

  /**
   * Reveal tabs block.
   *
   * @param {Object} params - animation settings
   * @returns {Animation}
   */
  static revealTabs(params) {

    // animate line
    if (params.$line.css('display') != 'none') {
      const lineWidth = params.$line.width();
      const lineLeft = params.$line.css('transform').split(',')[4];
      const fromX = params.fromX || '-1300px';
      const fromWidth = params.fromWidth || '1100px';

      TweenMax.fromTo(params.$line, 0.96,
        {
          css: { opacity: 1, width: fromWidth, x: fromX } },
        {
          ease: Power3.easeInOut,
          css: { opacity: 1, width: `${lineWidth}px`, x: `${lineLeft && lineLeft.trim()}px` }
        }
      );

      //animate select title
    } else {
      this.revealTextBlock(params.$dropdownTitle, {}, 1.6, 'anim_finished-64');
    }

    // tab's content
    this.tweenWithTimeout(params.$tabsContent, 0.72, {
      time: 0.88,
      params: {
        ease: Power2.easeInOut,
        css: { height: '100%' },
        onComplete: () => TweenMax.set(params.$tabsContent, { css: { height: '100%' } })
      }
    });

    // tab's titles
    this.delay(1.22, () => {
      this.staggerDelay(params.$tabsTitles, 0.2, (element) => {
        const cssClassName = 'anim_finished-30-none';

        $(element).addClass(cssClassName);
      });
    });

    // slider's buttons (if exist)
    if (params.$sliderButtons) {
      this.tweenWithTimeout(params.$sliderButtons, 1.6, {
        time: 0.64,
        params: { ease: Power2.easeInOut, css: { width: '161px' } }
      });
    }

    return this;
  }

  /**
   * Reveal tabs block.
   *
   * @param {jQuery} $board - board node
   * @param {Object} params - animation setting
   * @returns {Animation}
   */
  static revealBoard($board, params) {
    const $content = $board.find('.board__content');
    const $backdrop = $board.parents('section').find('.board__backdrop_inner');

    // show board, backdrop, content
    Animation.tweenWithTimeout($board, 0, {
      time: 1.1,
      params: {
        ease: Power3.easeInOut,
        css: { width: params.width },
        onComplete: () => Animation.revealTextBlock($content)
      }
    });
    Animation.tweenWithTimeout($backdrop, 0, {
      time: 1.1,
      params: {
        ease: Power3.easeInOut,
        css: { y: 0 }
      }
    });

    return this;
  }

  /**
   * Run home page's home screen reveal animation.
   *
   * @param {Object} params - animation settings
   * @returns {Promise}
   */
  static revealMainScreen(params) {
    return new Promise(resolve => {
      // show socials
      this.tweenWithTimeout(params.$socials, params.socialTimeout, {
        time: params.time.socials,
        params: {
          ease: params.ease.socials,
          css: { y: '100%' }
        }
      });

      // show home pic
      this.tweenWithTimeout(params.$sliderPic, params.sliderPicTimeout, {
        time: params.time.sliderPic,
        params: {
          ease: params.ease.sliderPic,
          css: { height: '100%' },
          onComplete: resolve
        }
      });

      // show hot section
      this.delay(params.hotTimeout, () => {
        // reveal slider
        TweenMax.to(params.$hotHidden, params.time.hot, {
          delay: params.hotTimeout,
          ease: params.ease.hot,
          css: { opacity: 1 }
        });
      });

      // show languages
      this.delay(params.languagesTimeout, () => {
        TweenMax.fromTo(params.$languages, params.time.languages, { css: { visibility: 'visible' } }, {
          ease: params.ease.languages,
          css: { y: '0%' }
        });
      });

      // show slider buttons
      this.tweenWithTimeout(params.$sliderButtons, params.sliderButtonsTimeout, {
        time: params.time.sliderButtons,
        params: {
          ease: params.ease.sliderButtons,
          css: params.sliderButtonsCss
        }
      });

      // show slider dots
      this.tweenWithTimeout(params.$sliderDots, params.sliderDotsTimeout, {
        time: params.time.sliderDots,
        params: {
          ease: params.ease.sliderButtons,
          css: params.sliderDotsCss,
          onComplete: () => {
            // remove overflow: hidden from dots to show absolutely positioned time
            params.$sliderDots.css('overflow', 'visible');
          }
        }
      });

      // show slider's 'time' DOM elements
      this.delay(params.sliderDotsTimeout - 0.2, () => {
        params.$sliderDotsTime.removeClass(params.sliderDotsTimeClass);
      });

      // show ornament
      this.tweenWithTimeout(params.$ornament, params.ornamentTimeout, {
        time: 0.5,
        params: { css: { opacity: 0.05 } }
      });

      // animate lines
      this.delay(params.mainScreenLinesTimeout, () => Animation.runLine(params.$mainScreenLines));

      // show static text
      this.delay(params.mainScreenTextTimeout, () => Animation.revealTextBlock(params.$mainScreenText()));

      // show title
      this.delay(params.mainScreenTitleTimeout, () => {
        Animation.revealSectionTitle(params.$mainScreenTitle, {
          dynamicText: params.mainScreenTitleActiveClass
        });
      });
    });
  }
}
