/**
 * Default revealing section.
 *
 * @module RevealingSection
 */

import { ActionOnScroll } from './_helpers';
import Animation from './Animation';

export default class RevealingSection {
  /**
   * Create revealing section instance.
   *
   * @param {jQuery} $section
   * @return {jQuery}
   */
  constructor($section) {
    this.$section = this.$anchor = $section;
    this.revealOffset = 50;
    this.revealOnMobile = false;
  }

  /**
   * Reveal animation steps.
   * Should return object with animation functions (maximum - 5 steps).
   *
   * @return {Object}
   */
  animationSteps() {

    return {
      one:   null,
      two:   null,
      three: null,
      four:  null,
      five:  null
    };
  }

  /**
   * Reveal section.
   *
   * @return {RevealingSection}
   */
  initRevealAnimation() {
    // get steps
    const animationSteps = this.animationSteps();

    // run animation
    Animation.steppedAnimation(animationSteps);

    return this;
  }

  /**
   * Initialize reveal animation on scroll.
   *
   * @return {RevealingSection}
   */
  initRevealOnScroll() {
    new ActionOnScroll({
      anchor: this.$anchor,
      action: this.initRevealAnimation,
      context: this,
      scrollOffset: this.revealOffset,
      onMobile: this.revealOnMobile
    });

    return this;
  };

  /**
   * Other section's scripts.
   *
   * @return {RevealingSection}
   */
  initOtherActions() { return this; }

  /**
   * Initialize section scripts.
   *
   * @return {RevealingSection}
   */
  init() {
    this
      .initRevealOnScroll()
      .initOtherActions();

    return this;
  }
}
