import { urlB64ToUint8Array, _csrf } from './_helpers';

/**
 * @class Push
 */
export default class Push {
  /**
   * Public key for push-messages (VAPID).
   *
   * @private
   * @type {String}
   */
  _applicationServerPublicKey = window.applicationServerPublicKey;

  /**
   * Object received after SW registration.
   *
   * @private
   * @type {Object|Null}
   */
  _swRegistration = null;

  /**
   * Push notifications are supported.
   *
   * @public
   * @type {Boolean}
   */
  pushIsSupported = ('PushManager' in window);

  /**
   * Serviceworkers and push workers are supported.
   *
   * @public
   * @type {Boolean}
   */
  pushServiceWorkerIsSupported =
    (window.ServiceWorkerRegistration && 'showNotification' in window.ServiceWorkerRegistration.prototype);

  /**
   * User refused push notifications.
   *
   * @public
   * @type {Boolean}
   */
  notificationsDenied = ('Notification' in window) ? window.Notification.permission === 'denied' : true;

  /**
   * Subscription status.
   *
   * @public
   * @type {Boolean}
   */
  isSubscribed = false;

  /**
   * Get subscription state.
   *
   * @public
   * @static
   * @get
   * @return {String}
   */
  static get isSubscribed() {
    return window.localStorage.getItem('subscription');
  }

  /**
   * Set subscription state.
   *
   * @public
   * @static
   * @set
   */
  static set setSubscription(state) {
    window.localStorage.setItem('subscription', state);
  }

  /**
   * Activate push notifications module.
   * Should be called as callback when service worker registration promise resolves.
   *
   * @public
   * @param {Object} [serviceWorkerReg]
   * @return {Push}
   */
  getServiceWorkerRegistration(serviceWorkerReg) {
    if (!this.pushServiceWorkerIsSupported) {
      console.warn('Notifications aren\'t supported.');
      return this;
    }

    if (this.notificationsDenied) {
      console.warn('The user has blocked notifications.');
      return this;
    }

    if (!this.pushIsSupported) {
      console.warn('Push messaging isn\'t supported.');
      return this;
    }

    if (Push.isSubscribed === 'true') return this;

    if (serviceWorkerReg) this._swRegistration = serviceWorkerReg;

    return this;
  }

  /**
   * Perform specified action.
   * Available actions:
   * 'subscribeUser', 'unsubscribeUser', 'updateNotifications'
   *
   * @public
   * @param {String} action
   * @return {Promise.<Push>}
   */
  dispatch(action) {
    const actionIsString = typeof action === 'string';
    if (!actionIsString) {
      return console.error(`Push: action should be a string: ${action}`);
    }

    const methodName = `_${action}`;
    const actionIsAvailable = !!this[methodName];
    if (!actionIsAvailable) {
      return console.error(`Push: undefined action "${action}"`);
    }

    const swRegistrationIsPassed = !(this._swRegistration === null);
    if (!swRegistrationIsPassed) {
      return console.error(
        `Push: first you should register service worker and call 'getServiceWorkerRegistration'
         instance method with serviceWorkerRegistration as an argument`
      );
    }
    
    return this[methodName].call(this);
  }

  /**
   * Subscribe user for push notifications.
   *
   * @private
   * @async
   * @return {Promise.<Push>}
   */
  async _subscribeUser() {
    const {
      _applicationServerPublicKey,
      _swRegistration,
      _updateNotifications
    } = this;
    const applicationServerKey = urlB64ToUint8Array(_applicationServerPublicKey);

    try {
      await _swRegistration.pushManager.subscribe({
        userVisibleOnly: true,
        applicationServerKey
      });

      await _updateNotifications();

    } catch (err) {
      console.log(`Failed to subscribe the user: ${err}`);
    }

    return this;
  }

  /**
   * Unsubscribe user from push notifications.
   *
   * @private
   * @async
   * @return {Promise.<Push>}
   */
  async _unsubscribeUser() {
    const { _swRegistration, _updateNotifications } = this;

    try {
      const subscription = await _swRegistration.pushManager.getSubscription();

      if (subscription) {
        await subscription.unsubscribe();
      }

      await _updateNotifications();

    } catch (err) {
      console.log(`Error unsubscribing ${err}`);
    }

    return this;
  }

  /**
   * Update notifications status and subscription on server.
   *
   * @private
   * @async
   * @return {Promise.<Push>}
   */
  async _updateNotifications() {
    const { _swRegistration, _updateSubscriptionOnServer } = this;

    try {
      const subscription = await _swRegistration.pushManager.getSubscription();

      await _updateSubscriptionOnServer(subscription);

      this.isSubscribed = !(subscription === null);

    } catch (err) {
      console.log(`Error updating notifications ${err}`);
    }

    if (this.isSubscribed) {
      console.log('User IS subscribed.');
    } else {
      console.log('User is NOT subscribed.');
    }

    return this;
  }

  /**
   * Send user subscription to server.
   *
   * @TODO: make ajax options abstract
   *
   * @private
   * @return {Promise.<Object>} server response
   */
  _updateSubscriptionOnServer(subscription) {
    return new Promise((resolve) => {
      $.ajax({
        type: 'post',
        dataType: 'json',
        url: '/push',
        data: {
          _csrf,
          subscription: JSON.stringify(subscription),
          event: subscription ? 'subscribe' : 'unsubscribe'
        },
        success: ({ action }) => console.log(`Server registration updated, user ${action}`),
        error: ({ statusText }) => console.log(`Error during server registration update: ${statusText}`),
        complete: (xhr, text) => {
          Push.setSubscription = text === 'success';
          resolve(xhr);
        }
      });
    });
  }
}
