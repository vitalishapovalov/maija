'use strict';

/**
 * Commonly used constants and functions.
 *
 * @module Helpers
 */

/**
 * Cache body DOM element.
 *
 * @constant
 * @type {jQuery}
 */
export const $body = $('body');

/**
 * Cache document.
 *
 * @constant
 * @type {jQuery}
 */
export const $document = $(document);

/**
 * Cache window.
 *
 * @constant
 * @type {jQuery}
 */
export const $window = $(window);

/**
 * Cache header.
 *
 * @constant
 * @type {jQuery}
 */
export const $header = $('header');

/**
 * Cache footer.
 *
 * @constant
 * @type {jQuery}
 */
export const $footer = $('footer');

/**
 * Elements for cross-browser window scroll.
 *
 * @constant
 * @type {jQuery}
 */
export const $scrolledElements = $('html, body');

/**
 * Breadcrumbs DOM element.
 *
 * @constant
 * @type {jQuery}
 */
export const $breadcrumbs = $body.find('.breadcrumbs');

/**
 * Save CSRF token.
 *
 * @type {String}
 * @private
 */
export const _csrf = $('meta[name="csrf-token"]').attr('content');

/**
 * Detect IE 11.
 *
 * @constant
 * @type {Boolean}
 */
export const isIE = !!window.MSInputMethodContext && !!document.documentMode;

/**
 * Detect mozilla (firefox).
 *
 * @constant
 * @type {Boolean}
 */
export const isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

/**
 * Detect safari browser.
 *
 * @constant
 * @type {Boolean}
 */
export const isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

/**
 * Detect iPad.
 *
 * @constant
 * @type {Boolean}
 */
export const isIpad = navigator.userAgent.match(/iPad/i) != null;

/**
 * Match media device indicator.
 */
export class Resp {
  /**
   * Get window's current width.
   *
   * @get
   * @static
   * @return {Number}
   */
  static get currWidth() {
    return window.innerWidth;
  }

  /**
   * Detect touch events.
   *
   * @get
   * @static
   * @return {Boolean}
   */
  static get isTouch() {
    return 'ontouchstart' in window;
  }

  /**
   * Detect large desktop device.
   *
   * @get
   * @static
   * @return {Boolean}
   */
  static get isDeskL() {
    return window.matchMedia(`(min-width: 1600px)`).matches;
  }

  /**
   * Detect desktop device.
   *
   * @get
   * @static
   * @return {Boolean}
   */
  static get isDesk() {
    return window.matchMedia(`(min-width: 1240px)`).matches;
  }

  /**
   * Detect tablet device.
   *
   * @get
   * @static
   * @return {Boolean}
   */
  static get isTablet() {
    return window.matchMedia(`(min-width: 768px) and (max-width: 1279px)`).matches;
  }

  /**
   * Detect mobile device.
   *
   * @get
   * @static
   * @return {Boolean}
   */
  static get isMobile() {
    return window.matchMedia(`(max-width: 767px)`).matches;
  }
}

/**
 * Detect current page.
 *
 * @constant
 * @type {String}
 */
export const currentPage = $body.find('main').data('page');

/**
 * Css class names.
 *
 * @constant
 * @type {Object}
 */
export const css = {
  active: 'active',
  error: 'has-error',
  menuActive: 'menu-active',
  headerHidden: 'header-hidden',
  textAnimationFinished: 'anim_finished',
  hasPreloader: 'has-preloader',
  noPreloader: 'no-preloader',
  hovered: 'hovered',
  isHiddenForAnimation: 'is-hidden-for-animation',
  reveal: 'js-reveal',
  preventHeaderToggle: 'prevent-header-toggle',
  footerSuccess: 'footer--success-message'
};

/**
 * Initialize slider on selected element with selected options.
 *
 * @type {Function}
 * @param {jQuery} $element
 * @param {Object} options
 */
export const initializeSlider = ($element, options) => $element.slick(options);

/**
 * Check specified item to be target of the event.
 *
 * @param {Object} e - Event object.
 * @param {jQuery} item - Item to compare with.
 * @returns {Boolean} - Indicate whether clicked target is the specified item or no.
 */
export const checkClosest = (e, item) => $(e.target).closest(item).length > 0;

/**
 * Generate string of random letters.
 *
 * @param {Number} length
 */
export const randomString = (length = 10) => Math.random().toString(36).substr(2, length > 10 ? length : 10);

/**
 * Toggle class on specified element on click.
 *
 * @param {jQuery} clickHandler
 * @param {jQuery} element
 */
export const toggleClass = (clickHandler, element) => {
  let $parent;

  if (element.hasClass('main_screen_languages__inner')) {
    $parent = element.parent();
  }

  function listener() {
    element.toggleClass(css.active);

    if ($parent) {
      $parent.toggleClass(css.active);
    }
  }

  clickHandler.on('click tap', listener);
};

/**
 * Remove active class if click outside the element.
 *
 * @param {jQuery} clickHandler - element's click handler (in case of dropdown)
 * @param {jQuery} element
 * @param {String} className - className to remove, 'active' by default
 */
export const hideOnBodyClick = (clickHandler, element, className = css.active) => {
  $body.on('click tap', (e) => {
    const notElement = !checkClosest(e, element);
    const notClickHandler = !checkClosest(e, clickHandler);
    const elementIsActive = element.hasClass(className);

    if (notClickHandler && notElement && elementIsActive) {
      element.removeClass(className);

      const $parent = element.parent();
      if ($parent.hasClass('main_screen_languages')) {
        $parent.removeClass(className);
      }
    }
  });
};

/**
 * Initialize custom dropdown.
 *
 * @param {jQuery} clickHandler - Default click event handler to activate item.
 * @param {jQuery} element
 */
export const initCustomDropdown = (clickHandler, element) => {
  toggleClass(clickHandler, element);
  hideOnBodyClick(clickHandler, element);
};

/**
 * Initialize 'select one element from list'
 *
 * @param {jQuery} $element
 * @param {jQuery} $selected
 * @param {jQuery} $clickHandler
 * @param {Boolean} [closeSelect=false] - close dropdown after user select
 */
export const initSelect = ($element, $selected, $clickHandler, closeSelect = false) => {
  $clickHandler.on('click tap', function () {
    // change text
    $selected.text($(this).text());

    // close dropdown
    if (closeSelect) $element.removeClass(css.active);
  });
};

/**
 * Check if element is in viewport.
 *
 * @param {jQuery} $element
 * @param {Boolean} [fullyInView = false] - element should be fully in viewport?
 * @param {Number} [offsetTop = 0]
 * @returns {Boolean}
 */
export const isScrolledIntoView = ($element, offsetTop = 0, fullyInView = false) => {
  const pageTop = $window.scrollTop();
  const pageBottom = pageTop + $window.height();
  const elementTop = $element.offset().top + offsetTop;
  const elementBottom = elementTop + $element.height();

  if (fullyInView === true) {
    return ((pageTop < elementTop) && (pageBottom > elementBottom));
  } else {
    return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
  }
};

/**
 * Returns a function, that, as long as it continues to be invoked, will not be triggered.
 *
 * @param {Function} func
 * @param {Object} [context = null]
 * @param {Number} [wait = 250]
 * @param {Boolean} [immediate]
 * @returns {Function}
 */
export const debounce = (func, context = null, wait = 250, immediate) => {
  let timeout;

  return () => {
    const args = arguments;

    const later = () => {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };

    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

/**
 * Convert base64string to uint8array.
 *
 * @param base64String
 * @return {Uint8Array}
 */
export const urlB64ToUint8Array = base64String => {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
};

/**
 * Throttle function.
 *
 * @param {Function} fn
 * @param {Number} [threshold]
 * @param {Object} [scope]
 * @return {Function}
 */
export function throttle(fn, threshold = 250, scope) {
  let last, deferTimer;

  return function () {
    const context = scope || this;

    let now = +new Date,
      args = arguments;
    if (last && now < last + threshold) {
      clearTimeout(deferTimer);
      deferTimer = setTimeout(function () {
        last = now;
        fn.apply(context, args);
      }, threshold);
    } else {
      last = now;
      fn.apply(context, args);
    }
  };
}

/**
 * Register onscroll event with specified params.
 */
export class ActionOnScroll {
  /**
   * @param {Object|Array} params
   * @param {jQuery} params.anchor
   * @param {Function} params.action
   * @param {Boolean} [params.onMobile = false]
   * @param {Boolean} [params.once = true]
   * @param {Boolean} [params.fireOnLoad = false]
   * @param {Number} [params.fireOnLoadTimeout = 0]
   * @param {Object} [params.context = window]
   * @param {String} [params.eventNameSpace = random string]
   * @param {Number} [params.scrollOffset = 0 on mobile, 120 on others]
   * @param {Number} [params.throttleThreshold = 75]
   */
  constructor(params) {
    // if array was passed
    if (Array.isArray(params)) {
      for (const param of params) {
        new ActionOnScroll(param);
      }
      return;
    }

    params = this.params = {
      anchor: params.anchor,
      action: params.action,
      onMobile: typeof params.onMobile === 'boolean' ? params.onMobile : false,
      once: typeof params.once === 'boolean' ? params.once : true,
      fireOnLoad: typeof params.fireOnLoad === 'boolean' ? params.fireOnLoad : false,
      fireOnLoadTimeout: params.fireOnLoadTimeout || 0,
      context: params.context || window,
      eventNameSpace: params.eventNameSpace || randomString(Math.floor(Math.random() * 100)),
      scrollOffset: params.scrollOffset || Resp.isMobile ? 0 : 120,
      throttleThreshold: params.throttleThreshold || 75
    };

    // don't run on mobile
    if (!params.onMobile && Resp.isMobile) return;

    this.init(params);
  }

  /**
   * Handle onscroll event with debounce.
   *
   * @param {Object} params
   * @returns {ActionOnScroll}
   */
  handleOnScroll(params = this.params) {
    // Fire action on load (fireOnLoad flag was set)
    if (params.fireOnLoad) {
      setTimeout(() => params.action.call(params.context), params.fireOnLoadTimeout);
      return this;
    }

    // ensure that function wont be called multiple times
    let funcWasCalled = false;

    const actionFunction = throttle(() => {
      const $anchor = typeof params.anchor === 'function' ? params.anchor() : params.anchor;

      if (isScrolledIntoView($anchor, params.scrollOffset) && !funcWasCalled) {
        // call function
        params.action.call(params.context);

        // set 'called' flag
        funcWasCalled = true;

        // don't listen for scroll event anymore (unregister)
        if (params.once) $window.unbind(`scroll.${params.eventNameSpace}`);
      }
    }, params.throttleThreshold, params.context);

    // register event
    $window.on(`scroll.${params.eventNameSpace}`, actionFunction);

    return this;
  }

  /**
   * Check params for required elements.
   *
   * @param {Object} params
   * @returns {ActionOnScroll}
   */
  checkParams(params) {
    if (!params.action) {
      console.error('No params are attached.');
    } else if (!params.anchor) {
      console.error('No anchor attached.');
    }

    return this;
  }

  /**
   * Initialize events.
   *
   * @param {Object|Array} params
   */
  init(params) {
    if (typeof params === 'object') {
      this.checkParams(params).handleOnScroll(params);
    } else {
      console.error('Only array of objects or object are supported.');
    }
  }
}

/**
 * Bind specified method's context.
 *
 * @param {String} methods
 */
export const bindMethods = function (...methods) {
  methods.forEach(method => this[method] = this[method].bind(this));
};

/**
 * Invoke function only once.
 *
 * @param func
 * @param context
 * @return {Function}
 */
export const once = function (func, context = global) {
  let result, alreadyCalled = false;

  return function () {
    if (!alreadyCalled) {
      result = func.apply(context, arguments);
      alreadyCalled = true;
    }
    return result;
  };
};

/**
 * Find active elements.
 *
 * @return {jQuery|Boolean} active elements, false if no active elements found
 */
$.fn.active = function () {
  const active = `.${css.active}`;
  const activeRowElements = this.filter(active);
  const activeChildElements = this.find(active);

  if (activeRowElements.length) return activeRowElements;

  if (activeChildElements.length) return activeChildElements;

  return false;
};

/**
 * Promise delay.
 *
 * @return {Promise}
 */
export const delay = time => new Promise(resolve => setTimeout(resolve, time));

/**
 * Validate email.
 *
 * @param {String} email
 * @return {Boolean}
 */
export const validateEmail = (email) => {
  const pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

  return pattern.test(email);
};

/**
 * Get current minutes rounded to some value
 *
 * @param roundValue
 * @returns {number}
 */
export const getRoundetMinutes = (roundValue) => {
  let date = new Date();
  let curMinutes = date.getMinutes();
  curMinutes = (roundValue * Math.ceil(curMinutes / roundValue));

  return curMinutes;
}
