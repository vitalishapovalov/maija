import PushNotifications from './PushNotifications';
import { currentPage } from './_helpers';

/**
 * @class PWA
 */
export default class PWA {
  /**
   * @constructor
   * @param {String} swFilePath
   * @param {Object} [params]
   */
  constructor(swFilePath, params = {}) {
    /**
     * User-specified options.
     *
     * @private
     * @type {Object}
     */
    this._params = params;

    /**
     * Path to the serviceworker.js file.
     *
     * @private
     * @type {String}
     */
    this._pathToSW = swFilePath;

    /**
     * Push notifications module instance.
     *
     * @private
     * @type {PushNotifications}
     */
    this._Push = new PushNotifications();

    /**
     * Detect ServiceWorker support
     *
     * @public
     * @type {Boolean}
     */
    this.serviceWorkerIsSupported = ('serviceWorker' in navigator);
  }

  /**
   * Initialize PWA module.
   *
   * @public
   * @return {PWA}
   */
  init() {
    window.addEventListener('load', () => {
      this
        ._deferPrompt()
        ._registerServiceWorker();
    });

    return this;
  }

  /**
   * Register service worker.
   *
   * @private
   * @async
   * @returns {Promise.<PWA>}
   */
  async _registerServiceWorker() {

    if (!this.serviceWorkerIsSupported) return this;

    // wait 12 sec to make sure that animation is finished
    if (currentPage === 'home') {
      await new Promise(resolve => setTimeout(resolve, 12000));
    }

    const { serviceWorker } = navigator;
    let swRegistration;

    try {
      swRegistration = await serviceWorker.register(this._pathToSW);

    } catch (err) {
      console.error('Error during service worker registration:', err);
      return this;
    }

    // Updatefound is fired if sw file changes
    swRegistration.onupdatefound = () => {
      // The updatefound event implies that reg.installing is set
      const installingWorker = swRegistration.installing;

      installingWorker.onstatechange = () => {
        switch (installingWorker.state) {
          case 'installed':
            if (serviceWorker.controller) {
              // At this point, the old content will have been purged and the fresh content will
              // have been added to the cache.
              console.log('New or updated content is available.');
            } else {
              // At this point, everything has been precached.
              console.log('Content is now available offline.');
            }
            break;

          case 'redundant':
            console.error('The installing service worker became redundant.');
            break;
        }
      };
    };

    this._enablePushNotifications();

    return this;
  }

  /**
   * Enable push notifications.
   *
   * @private
   * @return {PWA}
   */
  _enablePushNotifications() {

    // enable push notifications only on some pages
    const unsupportedPages = [
      'home',
      'maija',
      'franchise',
      'search-results',
      'offline'
    ].join(' ');
    const pageIsUnsupported = unsupportedPages.includes(currentPage);

    if (pageIsUnsupported) return this;

    // Run callback when SW is registered
    // with 7.5s delay, to make sure that the user is interested
    setTimeout(
      async () => {
        const finishedSwRegistration = await navigator.serviceWorker.ready;

        this._Push
          .getServiceWorkerRegistration(finishedSwRegistration)
          .dispatch('subscribeUser');
      },
      7500
    );

    return this;
  }

  /**
   * Defer prompt of app install banner.
   *
   * @private
   * @returns {PWA}
   */
  _deferPrompt() {
    let deferredPrompt;

    window.addEventListener('beforeinstallprompt', e => {
      e.preventDefault();

      // Stash the event so it can be triggered later.
      deferredPrompt = e;

      // Show app install banner when user first scrolls
      window.addEventListener('scroll', promptEventListener);

      return false;
    });

    function promptEventListener() {
      if (deferredPrompt !== undefined) {
        // The user has had a positive interaction with our app and Chrome
        // has tried to prompt previously, so let's show the prompt.
        deferredPrompt.prompt();

        // Remove event listener
        window.removeEventListener('scroll', promptEventListener);
      }
    }

    return this;
  }
}
