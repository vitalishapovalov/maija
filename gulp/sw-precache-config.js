module.exports = {
  dynamicUrlToDependencies: {
    '/': ['src/template/pages/home.pug']
  },
  staticFileGlobs: [
    'www/fonts/**/**.*',
    'www/css/**.css',
    'www/img/**.*',
    'www/pic/**.*',
    'www/js/**.js'
  ],
  stripPrefix: 'www/',
  verbose: true,
  runtimeCaching: [{
    urlPattern: /this\\.is\\.a\\.regex/,
    handler: 'networkFirst'
  }]
};