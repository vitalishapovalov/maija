'use strict';

const gulp        = require('gulp');
const swPrecache  = require('sw-precache');
const cacheConfig = require('../sw-precache-config');

gulp.task('service-worker', callback => {
  swPrecache.write('www/cache-worker.js', cacheConfig, callback);
});