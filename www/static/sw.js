'use strict';

const CACHE_VERSION = 4;
const PATH_TO_DEFAULT_ICON = 'icon-512.png';
const OFFLINE_URL = '/offline';
const CURRENT_CACHES = {
  offline: 'offline-v' + CACHE_VERSION
};
const ASSETS_TO_CACHE = [
  '/static/js/index.min.js',
  '/static/css/index.min.css',
  '/fonts/Futura_PT_Bold/futura-pt-bold-587e2b28316ed.woff',
  '/fonts/Helvetica/HelveticaNeue.woff'
];

function createCacheBustedRequest(url) {
  let request = new Request(url, { cache: 'reload' });

  if ('cache' in request) return request;

  const bustedUrl = new URL(url, self.location.href);
  bustedUrl.search += (bustedUrl.search ? '&' : '') + 'cachebust=' + Date.now();

  return new Request(bustedUrl);
}

self.addEventListener('install', event => {
  event.waitUntil(
    self.skipWaiting().then(() => {
      fetch(createCacheBustedRequest(OFFLINE_URL)).then((response) => {
        return caches.open(CURRENT_CACHES.offline).then((cache) => {
          return cache
            .put(OFFLINE_URL, response)
            .then(() => cache.addAll(ASSETS_TO_CACHE));
        });
      });
    })
  );
});

self.addEventListener('activate', event => {
  const expectedCacheNames = Object.keys(CURRENT_CACHES).map((key) => CURRENT_CACHES[key]);

  event.waitUntil(
    caches.keys().then(cacheNames => {
      return Promise.all(
        cacheNames.map(cacheName => {
          if (expectedCacheNames.indexOf(cacheName) === -1) {
            console.log('Deleting out of date cache:', cacheName);
            return caches.delete(cacheName);
          }
        })
      );
    })
  );
});

self.addEventListener('fetch', event => {
  if (event.request.mode === 'navigate' ||
    (event.request.method === 'GET' &&
      event.request.headers.get('accept').includes('text/html'))) {
    console.log('Handling fetch event for', event.request.url);
    event.respondWith(
      fetch(event.request).catch(error => {
        console.log('Fetch failed; returning offline page instead.', error);
        return caches.match(OFFLINE_URL);
      })
    );
  } else {
    event.respondWith(
      caches.match(event.request)
        .then(response => response || fetch(event.request))
    );
  }
});

self.addEventListener('push', (event) => {
  event.waitUntil(
    fetch('/fetch').then((response) => {
      if (response.status !== 200) {
        console.log('Looks like there was a problem. Status Code: ' + response.status);
        throw new Error();
      }

      return response.json().then((data) => {
        if (data.error || !data.notification) {
          console.error('The API returned an error.', data.error);
          throw new Error();
        }

        const title = data.notification.title;

        return self.registration.showNotification(title, {
          body: data.notification.body,
          icon: PATH_TO_DEFAULT_ICON,
          data: {
            url: data.notification.url
          }
        });
      }).catch((err) => {
        console.error('Unable to retrieve data', err);

        const title = 'An error occurred';
        const message = 'We were unable to get the information for this push message';
        const notificationTag = 'notification-error';

        return self.registration.showNotification(title, {
          body: message,
          icon: PATH_TO_DEFAULT_ICON,
          tag: notificationTag
        });
      });
    })
  );
});

self.addEventListener('notificationclick', (event) => {
  event.notification.close();

  const url = event.notification.data.url;

  event.waitUntil(clients.openWindow(url));
});
